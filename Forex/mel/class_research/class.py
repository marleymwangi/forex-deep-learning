    import pandas as pd
    import numpy as np
    from scipy.signal import argrelextrema
    import matplotlib.pyplot as plt

    def classify(df):

        # intializing variables
        call = 0
        pip_size = 10000
        state = 'free'

        calll = []
        statel = []

        # copy to preserve main df integrity
        dftemp = df.copy()

        # print(dftemp.head())
        # add columns to look at 1 step before now and 4 steps ito future for classification
        for item in dftemp:
            for count in range(-4, 2):
                name = 'shift' + str(count)
                dftemp[name] = dftemp.close.shift(count)
        # print(dftemp.head())

        # iterate thorugh each row and for each get price changes between now and each of the 4 future steps
        for i, row in dftemp.iterrows():

            prevp = row['shift1']
            currp = row['shift0']
            next5 = row['shift-1']
            next10 = row['shift-2']
            next15 = row['shift-3']
            next20 = row['shift-4']

            change = prevp - currp
            change1 = next5 - currp
            change2 = next10 - next5
            change3 = next15 - next10
            change4 = next20 - next15

            # checks for price increment for the next 2 steps: buy and hold
            if change1 > 0 and change2 > 0 and (abs(change1) + abs(change2) > (3 / pip_size)):
                if state != 'bhold':
                    call = 1
                    state = 'bhold'
                elif state == 'bhold':
                    call = 0
                    state = 'bhold'
            # next step price increases then falls but the rise was 3 pips more than the fall or initial rise is more than 5 pips: buy and hold
            elif (change1 > 0 and change2 < 0 and (abs(change1) - abs(change2) > (3 / pip_size))) or change1 > (5 / pip_size):
                if state != 'bhold':
                    call = 1
                    state = 'bhold'
                elif state == 'bhold':
                    call = 0
                    state = 'bhold'
            # checks for price fall for the next 2 steps: sell and hold
            elif change1 < 0 and change2 < 0 and (abs(change1) + abs(change2) > (3 / pip_size)):
                if state != 'shold':
                    call = -1
                    state = 'shold'
                elif state == 'shold':
                    call = 0
                    state = 'shold'
            # next step price falls then increases but the fall was 3 pips more than the rise or initial fall is more than 5 pips: sell and hold
            elif (change1 < 0 and change2 > 0 and (abs(change1) - abs(change2) > (3 / pip_size))) or change1 < (5 / pip_size):
                if state != 'shold':
                    call = -1
                    state = 'shold'
                elif state == 'shold':
                    call = 0
                    state = 'shold'
            else:
                # help handling outliers
                call = 0

            # appends calls and state to the lists
            calll.append(call)
            statel.append(state)

        # adds them to dataframe copy(state mostly used to counter check calls... also see class.py maindir )
        dftemp['call'] = calll
        dftemp['state'] = statel
        # print(dftemp[['shift1', 'shift0', 'shift-1', 'shift-2', 'shift-3', 'shift-4', 'call', 'state']].iloc[0:6])

        return dftemp.call

    # imprt df
    df = pd.read_csv('data/training/AUDJPY-Hour.csv', parse_dates=True, na_values=['nan'], usecols=['datetime', 'close'])

    df.datetime = pd.to_datetime(df.datetime, format='%Y.%m.%d %H:%M:%S.%f')

    df.drop_duplicates()

    df = df.set_index(df.datetime)

    df = df.iloc[-3000:]
    df['call'] = classify(df)

    df = df[['close', 'call']]

    call = df.call.values
    price = df.close
    buyl = []
    selll = []

    for i in range(0, len(price)):
        if call[i] == 1:
            buyl.append(i)
        if call[i] == -1:
            selll.append(i)

    # print(buyl)
    # print(selll)

    buyp = price.values[buyl]
    sellp = price.values[selll]

    plt.plot(price.values)
    plt.scatter(buyl, buyp, c='g')
    plt.scatter(selll, sellp, c='r')
    plt.show()
