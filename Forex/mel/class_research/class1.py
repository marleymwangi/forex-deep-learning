import pandas as pd
import numpy as np
from scipy.signal import argrelmax, argrelmin
import matplotlib.pyplot as plt
import time
from tqdm import tqdm


def classify(df):

    call = 0
    state = 'free'
    calll = []
    statel = []

    dftemp = df.copy()
    ordernum = 3
    price = dftemp.ema2

    print('\n\tStarting Classification of {} samples\n'.format(int(len(dftemp))))

    sellind = argrelmax(price.values, order=ordernum)[0]
    buyind = argrelmin(price.values, order=ordernum)[0]

    print('\n\tClassification Complete\n')
    calls = np.zeros(len(dftemp))

    for sell in sellind:
        calls[sell] = -1

    for buy in buyind:
        calls[buy] = 1

    dftemp['call'] = calls

    return dftemp.call


def validate_calls(df):
    dftemp1 = df.copy()
    prev_buy = 0
    prev_sell = 0
    call_ind = 0
    state = 'free'
    thresh = 0.0050
    statel = []
    '''
    b_en = buy entry
    b_ex = buy exit
    s_en = sell entry
    s_ex = sell exit
    '''
    for index, row in dftemp1.iterrows():
        row_call = row['call']
        row_close = row['ema2']

        if row_call == 1:
            if state == 'free':
                state = 'b_en'
                prev_buy = row_close
                call_ind = index

            elif state == 'b_en':
                dftemp1['call'][call_ind] = 0
                prev_buy = row_close
                call_ind = index

            elif state == 'b_ex':
                state = 'b_en'
                prev_buy = row_close
                call_ind = index

            elif state == 's_en':
                if prev_sell - row_close > thresh:
                    state = 'b_ex'
                    prev_buy = row_close
                    call_ind = index
                else:
                    dftemp1['call'][call_ind] = 0
                    state = 'b_en'
                    prev_buy = row_close
                    call_ind = index
            elif state == 's_ex':
                if prev_sell - row_close > thresh:
                    state = 'b_ex'
                    prev_buy = row_close
                    call_ind = index
                else:
                    state = 'b_en'
                    prev_buy = row_close
                    call_ind = index

        elif row_call == -1:
            if state == 'free':
                state = 's_en'
                prev_sell = row_close
                call_ind = index
            elif state == 's_en':
                prev_sell = row_close
                dftemp1['call'][call_ind] = 0
                call_ind = index
            elif state == 's_ex':
                state = 's_en'
                prev_sell = row_close
                call_ind = index
            elif state == 'b_en':
                if row_close - prev_buy > thresh:
                    state = 's_ex'
                    prev_sell = row_close
                    call_ind = index
                else:
                    dftemp1['call'][call_ind] = 0
                    state = 's_en'
                    prev_sell = row_close
                    call_ind = index
            elif state == 'b_ex':
                if row_close - prev_buy > thresh:
                    state = 's_ex'
                    prev_sell = row_close
                    call_ind = index
                else:
                    state = 's_en'
                    prev_sell = row_close
                    call_ind = index

        statel.append(state)

    dftemp1['state'] = statel

    print(dftemp1)

    return dftemp1.call, dftemp1.state


# imprt df
df = pd.read_csv('data/training/EURUSD-Minute5.csv', parse_dates=True, na_values=['nan'], usecols=['datetime', 'ema2'])

df.datetime = pd.to_datetime(df.datetime, format='%Y.%m.%d %H:%M:%S.%f')

df.drop_duplicates()

df = df.set_index(df.datetime)
df.drop('datetime', axis=1, inplace=True)

df = df.iloc[-10000:]
# print(df)

df['call'] = classify(df)
#df['call'], df['state'] = validate_calls(df)

dftemp = df.copy()

df = df[['ema2', 'call']]

# print(df)

call = df.call.values
price = df.ema2
buyl = []
selll = []


for i in range(0, len(price)):
    if call[i] == 1:
        buyl.append(i)
    if call[i] == -1:
        selll.append(i)

# print(buyl)
# print(selll)

buyp = price.values[buyl]
sellp = price.values[selll]

plt.plot(price.values)
plt.scatter(buyl, buyp, c='g')
plt.scatter(selll, sellp, c='r')
plt.show()
