import pandas as pd
import numpy as np
import sys
import shutil
from file_man.featext import *
# from iq import *

# import libraries for neural net
from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Reshape
from keras.layers import LSTM, SimpleRNN, Conv2D, MaxPooling2D
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.optimizers import Adam
from keras.layers import advanced_activations

# import libraries for ML
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.externals import joblib
from sklearn.metrics import classification_report
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis

retrain_text = """
\tRetrain Model code:\t[Model] [ Save ] [ Retrain ]:
\t\t\t1: Yes\t 1: Yes
\t\t\t2: No\t 2: No
\t\t\t3: blank
\t\t\tModel codes
\t1:RNN 2:CNN 3:DEEP_ALL 4:SGD 5:SVM 6:LREG 7:LDA 8:SKL 9:ALL 0:NOTHING

\tRetrain Model:"""


# checks if correct number of arguments are passed ..if not prompt
if len(sys.argv) != 6:
    print(usage)
    exit()

train_int, visual, pred_bool, retrain_bool, model_testing_bool = int(sys.argv[1]), int(sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5])

visual = True if visual == 1 else False
pred_bool = True if pred_bool == 1 else False
retrain_bool = True if retrain_bool == 1 else False
model_testing_bool = True if model_testing_bool == 1 else False
visual_pred = True if pred_bool and visual else False
if pred_bool and visual:
    visual = False


def build_rnn_model():
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    nodes = 128
    activationChoice = 'relu'
    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add(LSTM(nodes, input_shape=(None, x_train.shape[2]), return_sequences=True))
    model.add(Dropout(d))
    model.add(LSTM(int(nodes * 0.5), return_sequences=True))
    model.add(Dropout(d))
    model.add(LSTM(nodes, return_sequences=False))
    model.add(Dropout(d))
    model.add(Dense(nodes, activation=activationChoice))  # ,kernel_initializer='uniform'
    model.add(Dense(int(nodes * 0.5), activation=activationChoice))
    model.add(Dense(int(nodes * 0.5), activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))
    model.summary()

    optimizer = Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['accuracy'])  # para = ,metrics=['accuracy']

    print('\n\tCreated RNN model\n')

    return model


def build_cnn_model():
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    activationChoice = 'relu'

    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add((Conv2D(filters=32, kernel_size=(3, 3), activation=activationChoice, input_shape=(x_train.shape[1], x_train.shape[2], 1))))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Conv2D(64, (1, 3)))
    model.add(MaxPooling2D(pool_size=(3, 4)))
    model.add(Flatten())
    model.add(Reshape((1, -1)))
    model.add(LSTM(64, input_shape=(None, 256), return_sequences=False))
    model.add(Dropout(d))
    model.add(Dense(16, activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))

    optimizer = Adam(lr=0.010)
    model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['accuracy'])  # para = ,metrics=['accuracy']
    model.summary()

    print('\n\tCreated CNN LSTM model\n')

    return model


def train_rnn():
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(filepath='logs/{}/rnn_weights.hdf5'.format(symbol), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=False, write_images=True)  # tensorboard --logdir=Graph
    rnn_model.fit(x_train, y_train, batch_size=64, epochs=60, callbacks=[checkpointer, tbCallBack], validation_data=(x_test, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = rnn_model.predict(x_test)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print('\n\tRNN Precision Recall and F1\n')
    print(report)


def train_cnn():
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(filepath='logs/{}/cnn_weights.hdf5'.format(symbol), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=False, write_images=True)  # tensorboard --logdir=Graph
    cnn_model.fit(x_train_cnn, y_train, batch_size=64, epochs=1000, callbacks=[checkpointer, tbCallBack], validation_data=(x_test_cnn, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = cnn_model.predict(x_test_cnn)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print('\n\tCNN Precision Recall and F1\n')
    print(report)


def train_svm():
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Linear SVC classifier
    clf = LinearSVC(class_weight=class_weights)  # , loss='hinge'
    print('\n\tCreated {} Linear SVC model\n'.format(symbol))
    print('\n\tTraining {} Linear SVC\n'.format(symbol))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print('\n\t {} Linear SVC accuracy is {}\n'.format(symbol, accuracy))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print('\n\tLinear SVC Precision Recall and F1\n')
    print(report)
    '''
    print(y_test_skl[0:10])
    print(predicted[0:10])
    print(clf.predict_proba(x_test_skl[0:10]))
    '''
    # save the trained weight values
    joblib.dump(clf, 'logs/{}/svm.pkl'.format(symbol))
    print('\n\t{} Linear SVC weights saved\n'.format(symbol))


def train_lreg():
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Logistic Regression classifier
    clf = linear_model.LogisticRegression(class_weight=class_weights)
    print('\n\tCreated {} Logistic Regression model\n'.format(symbol))
    print('\n\tTraining {} Logistic Regression\n'.format(symbol))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print('\n\t{} Logistic Regression accuracy is {}\n'.format(symbol, accuracy))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print('\n\tLogistic Regression Precision Recall and F1\n')
    print(report)

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/lreg.pkl'.format(symbol))
    print('\n\t{} Logistic Regression weights saved\n'.format(symbol))


def train_sgd():
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training SGD Classifier
    clf = linear_model.SGDClassifier(class_weight=class_weights, penalty='l1')
    print('\n\tCreated {} SGD Classifier model\n'.format(symbol))
    print('\n\tTraining {} SGD Classifier\n'.format(symbol))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print('\n\t{} SGD Classifier accuracy is {}\n'.format(symbol, accuracy))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print('\n\tSGDClassifier Precision Recall and F1\n')
    print(report)

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/sgd.pkl'.format(symbol))
    print('\n\t{} SGD Classifier weights saved\n'.format(symbol))


def train_lda():
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Linear Discriminant Analysis classifier
    clf = LinearDiscriminantAnalysis()
    print('\n\tCreated {} LinearDiscriminantAnalysis model\n'.format(symbol))
    print('\n\tTraining {} LinearDiscriminantAnalysis\n'.format(symbol))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print('\n\t{} LinearDiscriminantAnalysis accuracy is {}\n'.format(symbol, accuracy))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print('\n\tLinearDiscriminantAnalysis Precision Recall and F1\n')
    print(report)

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/lda.pkl'.format(symbol))
    print('\n\t{} LinearDiscriminantAnalysis weights saved\n'.format(symbol))


def train_qda():
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Quadratic Discriminant Analysis classifier
    clf = QuadraticDiscriminantAnalysis()
    print('\n\tCreated {} QuadraticDiscriminantAnalysis model\n'.format(symbol))
    print('\n\tTraining {} QuadraticDiscriminantAnalysis\n'.format(symbol))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print('\n\t{} QuadraticDiscriminantAnalysis accuracy is {}\n'.format(symbol, accuracy))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print('\n\tQuadraticDiscriminantAnalysis Precision Recall and F1\n')
    print(report)

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/qda.pkl'.format(symbol))
    print('\n\t{} QuadraticDiscriminantAnalysis weights saved\n'.format(symbol))


def load_rnn():
    rnn_model.load_weights('logs/{}/rnn_weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    rnn_model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print('\n\tLoaded {} RNN weights from file\n'.format(symbol))


def load_cnn():
    cnn_model.load_weights('logs/{}/cnn_weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    cnn_model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print('\n\tLoaded {} CNN weights from file\n'.format(symbol))


def load_svm():
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/svm.pkl'.format(symbol))
    print('\n\tLoaded {} Linear SVC weights from file\n'.format(symbol))
    return lda


def load_lreg():
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/lreg.pkl'.format(symbol))
    print('\n\tLoaded {} Logistic Regression weights from file\n'.format(symbol))
    return lda


def load_sgd():
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/sgd.pkl'.format(symbol))
    print('\n\tLoaded {} SGD Classifier weights from file\n'.format(symbol))
    return lda


def load_lda():
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/lda.pkl'.format(symbol))
    print('\n\tLoaded {} LinearDiscriminantAnalysis weights from file\n'.format(symbol))
    return lda


def load_qda():
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/qda.pkl'.format(symbol))
    print('\n\tLoaded {} QuadraticDiscriminantAnalysis weights from file\n'.format(symbol))
    return lda


def classwghtnn(y_train):
    # creates class weights using sklearn to deal with imbalanced data
    y_classes = []

    # Create a pd.series that represents the categorical class of each one-hot encoded row
    y_classes = np.argmax(y_train, axis=1)

    from sklearn.preprocessing import LabelEncoder
    # Instantiate the label encoder
    le = LabelEncoder()

    # Fit the label encoder to our label series
    le.fit(list(y_classes))

    # Create integer based labels Series
    y_integers = le.transform(list(y_classes))

    # Create dict of labels : integer representation
    labels_and_integers = dict(zip(y_classes, y_integers))

    from sklearn.utils.class_weight import compute_class_weight, compute_sample_weight

    class_weights = compute_class_weight('balanced', np.unique(y_integers), y_integers)
    sample_weights = compute_sample_weight('balanced', y_integers)

    class_weights_dict = dict(zip(le.transform(list(le.classes_)), class_weights))

    # print(class_weights_dict)

    return class_weights_dict


def classwghtskl(y_train_skl):
    from sklearn.utils.class_weight import compute_class_weight

    class_weight_list = compute_class_weight('balanced', np.unique(y_train_skl), y_train_skl)
    class_weights_dict = dict(zip(np.unique(y_train_skl), class_weight_list))

    # print(class_weights_dict)

    return class_weights_dict


def get_index(array):
    # convert RNN results to 1d array
    lst = []

    for item in array:
        index = np.argmax(item)
        if index == 0:
            lst.append(1)
        elif index == 1:
            lst.append(0)
        elif index == 2:
            lst.append(-1)
    return lst


def rshape(unreshaped):
    # reshape for plotly compatibility
    unreshaped = np.array(unreshaped)
    reshaped = np.reshape(unreshaped, (unreshaped.shape[0]))

    return reshaped


def cnn_rshape(pred, x_train, x_test=0):
    # reshape for cnn input ... Requires 4dim
    cnn_shape = (x_train.shape[1], x_train.shape[2], 1)
    x_train_cnn = x_train.reshape(x_train.shape[0], *cnn_shape)
    if not pred:
        x_test_cnn = x_test.reshape(x_test.shape[0], *cnn_shape)
        return x_train_cnn, x_test_cnn
    elif pred:
        return x_train_cnn


def predict_rnn(x_test, y_test, pred_bool):
    # Making RNN Predictions and returns the predictions

    # real values are needed for training visualisation but not prediction
    if not pred_bool:
        real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = rnn_model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    # since real values arent needed in pred
    if not pred_bool:
        return predicted_stock_price, real_pred
    elif pred_bool:
        return predicted_stock_price


def predict_cnn(x_test, y_test, pred_bool):
    # Making RNN Predictions and returns the predictions

    # real values are needed for training visualisation but not prediction
    if not pred_bool:
        real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = cnn_model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    # since real values arent needed in pred
    if not pred_bool:
        return predicted_stock_price, real_pred
    elif pred_bool:
        return predicted_stock_price


def predict_skl(x_test_skl, skl):
    # Getting the prediction from skl model passed
    predicted_stock_priceskl = skl.predict(x_test_skl)
    # reshape for plotly compatibility
    predicted_stock_priceskl = rshape(predicted_stock_priceskl)

    return predicted_stock_priceskl


def visualize(x_test_cnn, x_test, y_test, x_test_skl, price):
    # gets prediction and real values
    predicted_stock_pricernn, real_pred = predict_rnn(x_test, y_test, pred_bool)
    # gets prediction and real values
    predicted_stock_pricecnn, _ = predict_cnn(x_test_cnn, y_test, pred_bool)
    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(x_test_skl, svm)

    # Getting the prediction from sgd
    predicted_stock_pricesgd = predict_skl(x_test_skl, sgd)

    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(x_test_skl, lreg)

    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(x_test_skl, lda)

    # combines all ml predictions into one thing
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_sgd = predicted_stock_pricesgd[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnn == next_pred_svm == next_pred_sgd == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0
        combined_pred.append(position)

    # combined ML algo evaluation
    report = classification_report(real_pred, combined_pred)
    print('\n\tCombined Precision Recall and F1\n')
    print(report)

    # plotly things
    trace1 = go.Scatter(y=real_pred, name=symbol + ' Real stock pred')
    trace2 = go.Scatter(y=price, name=symbol + ' Actual price')
    trace3 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
    trace4 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
    trace5 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
    trace6 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
    trace7 = go.Scatter(y=predicted_stock_pricesgd, name=symbol + ' SGD predicted price')
    trace8 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
    trace9 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

    fig = tools.make_subplots(rows=9, cols=1, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 2, 1)
    fig.append_trace(trace3, 3, 1)
    fig.append_trace(trace4, 4, 1)
    fig.append_trace(trace5, 5, 1)
    fig.append_trace(trace6, 6, 1)
    fig.append_trace(trace7, 7, 1)
    fig.append_trace(trace8, 8, 1)
    fig.append_trace(trace9, 9, 1)

    py.offline.plot(fig, filename='plot.html')


def prediction(pred_nn, pred_cnn, pred_skl, pred_price, visual_pred):
    # gets prediction and real values
    predicted_stock_pricernn = predict_rnn(pred_nn, y_test, pred_bool)
    # gets prediction and real values
    predicted_stock_pricecnn = predict_cnn(pred_cnn, y_test, pred_bool)
    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(pred_skl, svm)

    # Getting the prediction from sgd
    predicted_stock_pricesgd = predict_skl(pred_skl, sgd)

    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(pred_skl, lreg)

    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(pred_skl, lda)

    # combines all ml predictions into one thing
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_sgd = predicted_stock_pricesgd[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnn == next_pred_svm == next_pred_sgd == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0
        combined_pred.append(position)

    outputFileName = "../calgo/{}.txt".format(symbol)
    with open(outputFileName, "w") as text_file:
        text_file.write("{0}".format(combined_pred[-1]))

    print('\t{} Next RNN pred {},Next CNN pred {}, Next SVM pred {}, Next SGD pred {}, Next lREG pred {}, Next lda pred {}. Conclusion is {}'.format(symbol, int(predicted_stock_pricernn[-1]), int(predicted_stock_pricecnn[-1]), int(predicted_stock_pricesvm[-1]), int(predicted_stock_pricesgd[-1]), int(predicted_stock_pricelreg[-1]), int(predicted_stock_pricelda[-1]), int(combined_pred[-1])))

    # plotly things
    if visual_pred:
        # plotly things
        trace1 = go.Scatter(y=pred_price, name=symbol + ' Actual price')
        trace2 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
        trace3 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
        trace4 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
        trace5 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
        trace6 = go.Scatter(y=predicted_stock_pricesgd, name=symbol + ' SGD predicted price')
        trace7 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
        trace8 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

        fig = tools.make_subplots(rows=8, cols=1, shared_xaxes=True, shared_yaxes=True)
        fig.append_trace(trace1, 1, 1)
        fig.append_trace(trace2, 2, 1)
        fig.append_trace(trace3, 3, 1)
        fig.append_trace(trace4, 4, 1)
        fig.append_trace(trace5, 5, 1)
        fig.append_trace(trace6, 6, 1)
        fig.append_trace(trace7, 7, 1)
        fig.append_trace(trace8, 8, 1)

        py.offline.plot(fig, filename='plot.html')


def model_testing(df, symbol):
    print('\n\tCreating model_testing pred list')
    modeldf = model_testing_dataslice(df)
    pred_nn = model_testing_nn_data(modeldf, symbol)
    pred_cnn = cnn_rshape(True, pred_nn)
    pred_skl, pred_price = model_testing_skl_data(modeldf, symbol)

    # gets prediction and real values
    predicted_stock_pricernn = predict_rnn(pred_nn, y_test, True)
    # gets prediction and real values
    predicted_stock_pricecnn = predict_cnn(pred_cnn, y_test, True)
    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(pred_skl, svm)

    # Getting the prediction from sgd
    predicted_stock_pricesgd = predict_skl(pred_skl, sgd)

    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(pred_skl, lreg)

    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(pred_skl, lda)

    # selecting last predictions from all machines
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_sgd = predicted_stock_pricesgd[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnn == next_pred_svm == next_pred_sgd == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0
        combined_pred.append(position)

    # plotly things
    trace1 = go.Scatter(y=pred_price, name=symbol + ' Actual price')
    trace2 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
    trace3 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
    trace4 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
    trace5 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
    trace6 = go.Scatter(y=predicted_stock_pricesgd, name=symbol + ' SGD predicted price')
    trace7 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
    trace8 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

    fig = tools.make_subplots(rows=8, cols=1, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 2, 1)
    fig.append_trace(trace3, 3, 1)
    fig.append_trace(trace4, 4, 1)
    fig.append_trace(trace5, 5, 1)
    fig.append_trace(trace6, 6, 1)
    fig.append_trace(trace7, 7, 1)
    fig.append_trace(trace8, 8, 1)

    py.offline.plot(fig, filename='plot.html')

    print('\n\tSaving model_testing pred list')
    clean_model_testing(symbol)
    print('\n\tBuys {} and Sells {}. {} trades in total'.format(combined_pred.count(1), combined_pred.count(-1), (combined_pred.count(1) + combined_pred.count(-1))))
    outputFileName = "modeltest/{}-predlist.txt".format(symbol)
    for i in range(len(combined_pred)):
        with open(outputFileName, "a") as text_file:
            text_file.write("{}\n".format(combined_pred[i]))
    print('\n\tSaved model_testing pred list')


def train_skl():
    # helper function to help with train function below
    train_svm()
    train_sgd()
    train_lreg()
    train_lda()


def train_all():
    # helper function to help with train function below
    train_rnn()
    train_cnn()
    train_skl()


def train_deep_all():
    # helper function to help with train function below
    train_rnn()
    train_cnn()


def train(option):
    switcher = {
        1: train_rnn,
        2: train_cnn,
        3: train_deep_all,
        4: train_sgd,
        5: train_svm,
        6: train_lreg,
        7: train_lda,
        8: train_skl,
        9: train_all}

    # Get the function from switcher dictionary
    func = switcher.get(option, lambda: None)
    # Execute the function
    func()

# loads all saved weights for use


def load():
    load_rnn()
    load_cnn()
    svm = load_svm()
    sgd = load_sgd()
    lreg = load_lreg()
    lda = load_lda()
    return svm, sgd, lreg, lda


def save_retrain(symbol, train_int):
    models = {4: 'sgd', 5: 'svm', 6: 'lreg', 7: 'lda'}
    model = models.setdefault(train_int, 'sgd')
    shutil.copy('logs/{}/{}.pkl'.format(symbol, model), 'logs/{}/back/{}.pkl'.format(symbol, models[train_int]))
    print('\n\t{} {} weights saved in backup\n'.format(symbol, model))


def cpy_back(symbol):
    shutil.copy('logs/{}/back/sgd.pkl'.format(symbol), 'logs/{}/sgd.pkl'.format(symbol))
    print('\n\t{} weights loaded from backup\n'.format(symbol))


# list of symbols to be used for training
if not pred_bool:
    symbols = ['AUDJPY', 'AUDUSD']
    #symbols = ['AUDJPY','AUDUSD','EURCHF','EURGBP','EURJPY','EURUSD','GBPJPY','GBPUSD','USDCHF', 'USDCAD','USDCHF', 'USDJPY']

# list of symbols to be used for prediction
if pred_bool:
    symbols = ['AUDJPY', 'AUDUSD']
    #symbols = ['AUDJPY-Minute5','AUDUSD-Minute5','EURCHF-Minute5','EURGBP-Minute5','EURJPY-Minute5','EURUSD-Minute5','GBPJPY-Minute5','GBPUSD-Minute5','USDCHF-Minute5', 'USDCAD-Minute5', 'USDJPY-Minute5','XAGUSD-Minute5']

# London Session - EURUSD,GBPUSD,USDJPY,AUDUSD,NZDUSD,USDCAD,USDCHF,EURJPY,GBPJPY,AUDJPY,EURGBP,EURCHF

times = ['-Minute5']

for symbol_ in symbols:
    for t in times:
        symbol = '{}{}'.format(symbol_, t)
        # gets df with data and features
        df = get_data(symbol, False)
        # load RNN data from featext
        x_train, x_test, y_train, y_test = get_data_nn_class(df, symbol)
        # load RNN data from featext
        x_train_skl, x_test_skl, y_train_skl, y_test_skl, price = get_data_skl(df, symbol)

        x_train_cnn, x_test_cnn = cnn_rshape(False, x_train, x_test)

        cnn_model = build_cnn_model()
        rnn_model = build_rnn_model()

        # save train_int cause retrain may change it
        if retrain_bool:
            train_int_save = train_int
        # Loop to retrain desired models
        while True:
            train(train_int)
            svm, sgd, lreg, lda = load()  # run model testing

            # Loop to retrain desired models
            if visual:
                v_slice = 600
                visualize(x_test_cnn[-v_slice:], x_test[-v_slice:], y_test[-v_slice:], x_test_skl[-v_slice:], price[-v_slice:])
                # play sound once training and visualization is done
                import winsound
                duration, freq = 400, 440  # duration - millisecond,frequency in Hz
                winsound.Beep(freq, duration)

            # exit loop is user doesn't want to retrain
            if not retrain_bool:
                break
            elif retrain_bool:
                # get user input for trainnum, save and retrain
                retrain_prompt = input(retrain_text)

                # save previous weights before retraining
                # saves sgd on first run since is changes alot
                save = int(str(retrain_prompt)[1])
                # save model in back if yes
                save_bool = True if save == 1 else False
                if save_bool:
                    save_retrain(symbol, train_int)

                # changes Train int for second run
                train_int = int(str(retrain_prompt)[0])

                # End loop if retrain is no
                retrain = int(str(retrain_prompt)[2])
                retrain_bool = True if retrain == 1 else False
                if retrain_bool:
                    print('\tRetraining model {}'.format(train_int))
                if train_int == 0 and retrain_bool == False and save_bool == False:
                    cpy_back(symbol)
                if not retrain_bool:
                    print('\n\tExiting {} retraining'.format(symbol))
                    break
        # run model testing
        if model_testing_bool:
            modeldf = get_data(symbol, False)
            model_testing(modeldf, symbol)
        # sets train_int back to original incase training multiple symbols
        if retrain_bool:
            train_int = train_int_save

if pred_bool:
    import datetime
    import csv
    import time  # for sleep

    try:
        while True:
            for t in times:
                if datetime.datetime.now().minute % int(t[7:]) == 0:
                    global start_min
                    start_min = datetime.datetime.now().minute
                    for symbol_ in symbols:
                        print("\n\nRunning {} min Pred:\nTime ".format(t[7:]))
                        print(time.strftime("%H:%M:%S"))
                        print("\n")
                        symbol = '{}{}'.format(symbol_, t)
                        df = get_data(symbol, True)
                        pred_nn = predict_nn_data(df, symbol)
                        pred_cnn = cnn_rshape(True, pred_nn)
                        pred_skl, pred_price = predict_skl_data(df, symbol)
                        v_slice = 600
                        prediction(pred_nn[-v_slice:], pred_cnn[-v_slice:], pred_skl[-v_slice:], pred_price[-v_slice:], visual_pred)
                        print("\n\nCompleted!\nTime ")
                        print(time.strftime("%H:%M:%S"))
                        time.sleep(10)
                    if datetime.datetime.now().minute == start_min:
                        time.sleep(60 - datetime.datetime.now().second)

    except KeyboardInterrupt:
        print('\n\nManual break by user \n\n')


# for testing prediction
'''
for symbol in symbols:
    df = get_data(symbol, True)
    dfy = df.iloc[:-100]
    for i in range(95,100):
        pred_nn = predict_nn_data(dfy[:i], symbol)
        pred_skl, pred_price = predict_skl_data(dfy[:i], symbol)
        prediction(pred_nn, pred_skl, pred_price, visual_pred)
        time.sleep(6)
'''

# normal trading many assets
'''
if datetime.datetime.now().minute % 5 == 0:
                start_min = datetime.datetime.now().minute
                print("\n\nRerunning code!\n\nTime ")
                print(time.strftime("%H:%M:%S"))
                print("\n")
                time.sleep(2)
                for symbol in symbols:
                    df = get_data(symbol, True)
                    pred_nn = predict_nn_data(df, symbol)
                    pred_skl, pred_price = predict_skl_data(df, symbol)
                    prediction(pred_nn, pred_skl, pred_price, visual_pred)
                    if datetime.datetime.now().minute == start_min:
                        time.sleep(60 - datetime.datetime.now().second)
                print("\n\nCompleted!\n\nTime ")
                print(time.strftime("%H:%M:%S"))
'''
