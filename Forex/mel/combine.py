import numpy as np

symbols = ['AUDJPY', 'AUDUSD', 'EURCHF', 'EURGBP', 'EURJPY', 'EURUSD', 'GBPJPY', 'GBPUSD', 'USDCAD', 'USDCHF', 'USDJPY']

for symbol in symbols:
    print('Adding {}'.format(symbol))
    if symbol == symbols[0]:
        symbol = symbol + '-Minute30'
        x_traincombnn = np.load('data/training/{}/{}nnxtr.npy'.format(symbol, symbol))
        x_testcombnn = np.load('data/training/{}/{}nnxte.npy'.format(symbol, symbol))
        y_traincombnn = np.load('data/training/{}/{}nnytr.npy'.format(symbol, symbol))
        y_testcombnn = np.load('data/training/{}/{}nnyte.npy'.format(symbol, symbol))

        x_traincombsk = np.load('data/training/{}/{}skxtr.npy'.format(symbol, symbol))
        x_testcombsk = np.load('data/training/{}/{}skxte.npy'.format(symbol, symbol))
        y_traincombsk = np.load('data/training/{}/{}skytr.npy'.format(symbol, symbol))
        y_testcombsk = np.load('data/training/{}/{}skyte.npy'.format(symbol, symbol))
        close = np.load('data/training/{}/{}skclose.npy'.format(symbol, symbol))
    else:
        symbol = symbol + '-Minute30'
        x_trainnn = np.load('data/training/{}/{}nnxtr.npy'.format(symbol, symbol))
        x_testnn = np.load('data/training/{}/{}nnxte.npy'.format(symbol, symbol))
        y_trainnn = np.load('data/training/{}/{}nnytr.npy'.format(symbol, symbol))
        y_testnn = np.load('data/training/{}/{}nnyte.npy'.format(symbol, symbol))

        x_trainsk = np.load('data/training/{}/{}skxtr.npy'.format(symbol, symbol))
        x_testsk = np.load('data/training/{}/{}skxte.npy'.format(symbol, symbol))
        y_trainsk = np.load('data/training/{}/{}skytr.npy'.format(symbol, symbol))
        y_testsk = np.load('data/training/{}/{}skyte.npy'.format(symbol, symbol))
        close = np.load('data/training/{}/{}skclose.npy'.format(symbol, symbol))

        x_traincombnn = np.append(x_traincombnn, x_trainnn, axis=0)
        x_testcombnn = np.append(x_testcombnn, x_testnn, axis=0)
        y_traincombnn = np.append(y_traincombnn, y_trainnn, axis=0)
        y_testcombnn = np.append(y_testcombnn, y_testnn, axis=0)

        x_traincombsk = np.append(x_traincombsk, x_trainsk, axis=0)
        x_testcombsk = np.append(x_testcombsk, x_testsk, axis=0)
        y_traincombsk = np.append(y_traincombsk, y_trainsk, axis=0)
        y_testcombsk = np.append(y_testcombsk, y_testsk, axis=0)

np.save('data/training/combined-Minute30/combined-Minute30nnxtr.npy', x_traincombnn)
np.save('data/training/combined-Minute30/combined-Minute30nnxte.npy', x_testcombnn)
np.save('data/training/combined-Minute30/combined-Minute30nnytr.npy', y_traincombnn)
np.save('data/training/combined-Minute30/combined-Minute30nnyte.npy', y_testcombnn)

np.save('data/training/combined-Minute30/combined-Minute30skxtr.npy', x_traincombsk)
np.save('data/training/combined-Minute30/combined-Minute30skxte.npy', x_testcombsk)
np.save('data/training/combined-Minute30/combined-Minute30skytr.npy', y_traincombsk)
np.save('data/training/combined-Minute30/combined-Minute30skyte.npy', y_testcombsk)
np.save('data/training/combined-Minute30/combined-Minute30skclose.npy', close)

print(x_traincombnn.shape)
