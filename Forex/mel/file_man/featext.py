import os
import pandas as pd
import numpy as np
import colorful
from file_man.featfor import *
#from featfor import *

from sklearn import preprocessing
from scipy.signal import argrelmax, argrelmin, find_peaks
from sklearn.model_selection import train_test_split
from sklearn.externals import joblib

pred_items = 12  # number of items in pred input. Picks last val numbers
items = pred_items - 1  # number of items in nn input = 1 + val
min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))
train_split = 0.4


def get_data(symbol, pred):
    '''
    para: symbol... corilates to csvfile name to be read. Use instrument and timeframe
    returns df with all instrument featuresand call positions for training
              pred... used to determine which folder we are getting data from... training
    or predicting
    '''

    # store classify in pickle cause its expensive to classify for each run
    create_folder_pickle(symbol)
    pickle_path = 'data/training/{}/{}.p'.format(symbol, symbol)

    '''
    # if we are predicting read data in predicting folder if not try find pickle is it
    doesn't exist classify and store it in a pickle
    '''
    if pred:
        dftemp = read_data(symbol, pred)
    else:
        try:
            dftemp = pd.read_pickle(pickle_path)
        except Exception as e:
            dftemp = read_data(symbol, pred)
            # classifies data
            dftemp['call'] = classify(dftemp, symbol)
            dftemp.fillna(0, inplace=True)

            # scales prices using close
            dftemp.open = (dftemp.close - dftemp.open)
            dftemp.high = (dftemp.close - dftemp.high)
            dftemp.low = (dftemp.close - dftemp.low)
            dftemp.wclose = (dftemp.close - dftemp.wclose)

            dftemp.ema1 = (dftemp.close - dftemp.ema1)
            dftemp.ema2 = (dftemp.close - dftemp.ema2)
            dftemp.ema3 = (dftemp.close - dftemp.ema3)
            dftemp.ema4 = (dftemp.close - dftemp.ema4)

            dftemp.hband = (dftemp.close - dftemp.hband)
            dftemp.mband = (dftemp.close - dftemp.mband)
            dftemp.lband = (dftemp.close - dftemp.lband)

            # stores classified df as a picke
            dftemp.to_pickle(pickle_path)

    print(colorful.green('\n\tLoaded {}.csv file\n'.format(symbol)))

    return dftemp


def min_max_fit(df, symbol, ml):
    # copy df to preserve integrity
    dftemp = df.copy()
    # using sklearn min max scaler for normalization and fit and save the norm values for later use
    scaler = min_max_scaler.fit(dftemp[dftemp.columns])
    create_folder(symbol)
    joblib.dump(scaler, 'logs/{}/min_max_scaler{}.pkl'.format(symbol, ml))


def min_max_load(df, symbol, ml):
    # copy df to preserve integrity
    dftemp = df.copy()
    # using sklearn min max scaler for normalization after loading saved norm values
    scaler = joblib.load('logs/{}/min_max_scaler{}.pkl'.format(symbol, ml))
    dftemp[dftemp.columns] = scaler.transform(dftemp[dftemp.columns])
    return dftemp


def classify(df, symbol):

    class_prom = {
        'AUDJPY': 0.2,
        'AUDUSD': 0.001
    }
    print(class_prom.get(symbol[:6]))
    print(symbol[:6])

    # copy df to preserve integrity
    dftemp = df.copy()
    ordernum = 5
    price = dftemp.close

    print(colorful.orange('\n\tStarting Classification of {} samples\n'.format(int(len(dftemp)))))

    [sellind, _] = find_peaks(price, prominence=class_prom.get(symbol[:6]))
    invertedprice = max(price) - price
    [buyind, _] = find_peaks(invertedprice, prominence=class_prom.get(symbol[:6]))

    print(colorful.green('\n\tClassification Complete\n'))
    calls = np.zeros(len(dftemp))

    for sell in sellind:
        calls[sell] = -1

    for buy in buyind:
        calls[buy] = 1

    dftemp['call'] = calls

    return dftemp.call


def get_data_nn_class(df, symbol):
    # copy df to preserve integrity
    dftemp = df.copy()
    try:
        x_train = np.load('data/training/{}/{}nnxtr.npy'.format(symbol, symbol))
        x_test = np.load('data/training/{}/{}nnxte.npy'.format(symbol, symbol))
        y_train = np.load('data/training/{}/{}nnytr.npy'.format(symbol, symbol))
        y_test = np.load('data/training/{}/{}nnyte.npy'.format(symbol, symbol))

    except Exception as e:
        print(colorful.orange('\n\tError occured loading dataset pickles {}'))
        print(colorful.red('\t{}\n'.format(e)))
        print(colorful.orange('\n\tCreating NN array for {}\n'.format(int(len(dftemp)))))
        # splits data before fiting
        train_size = (int(len(dftemp) * train_split))

        # drop close before min max scaling
        dftemp.drop('close', axis=1, inplace=True)

        # separates label from feature dataset
        dftempy = dftemp.call
        dftemp.drop('call', axis=1, inplace=True)

        min_max_fit(dftemp[0:train_size], symbol, 'nn')
        dftemp.iloc[0:train_size] = min_max_load(dftemp.iloc[0:train_size], symbol, 'nn')
        dftemp.iloc[train_size:len(dftemp)] = min_max_load(
            dftemp.iloc[train_size:len(dftemp)], symbol, 'nn')

        # converts sf to array
        x = dftemp.values
        y = dftempy.values

        x = np.nan_to_num(x, copy=False)
        y = np.nan_to_num(y, copy=False)

        # groups dataset features for nn prediction
        y = y[items:]
        y1 = []
        # creates one hot array for nn prediction
        for item in y:
            temp = np.zeros(3)
            if item == 1:
                temp[0] = 1
            elif item == 0:
                temp[1] = 1
            elif item == -1:
                temp[2] = 1
            y1.append(temp)
        # covert list to array
        y1 = np.array(y1)

        x1 = []
        temp = []
        count = 0
        # creates a multidimensional tensor allexamples by items+1 by features
        for item in x:
            temp.append(item)
            if count > (items - 1):
                x1.append(temp)
                temp = temp[1:]
            count += 1

        x2 = []
        for item in x1:
            item = item[::-1]
            x2.append(item)

        x2 = np.array(x2)

        x_train, x_test, y_train, y_test = train_test_split(
            x2, y1, test_size=(1 - train_split), shuffle=False)

        np.save('data/training/{}/{}nnxtr.npy'.format(symbol, symbol), x_train)
        np.save('data/training/{}/{}nnxte.npy'.format(symbol, symbol), x_test)
        np.save('data/training/{}/{}nnytr.npy'.format(symbol, symbol), y_train)
        np.save('data/training/{}/{}nnyte.npy'.format(symbol, symbol), y_test)

    print(colorful.green('\n\t' + symbol + ' Loading NN Dataset Completed \n'))

    return x_train, x_test, y_train, y_test


def get_data_skl(df, symbol):
    # copy df to preserve integrity
    dftemp = df.copy()
    try:
        x_train = np.load('data/training/{}/{}skxtr.npy'.format(symbol, symbol))
        x_test = np.load('data/training/{}/{}skxte.npy'.format(symbol, symbol))
        y_train = np.load('data/training/{}/{}skytr.npy'.format(symbol, symbol))
        y_test = np.load('data/training/{}/{}skyte.npy'.format(symbol, symbol))
        close = np.load('data/training/{}/{}skclose.npy'.format(symbol, symbol))

    except Exception as e:
        print(colorful.orange('\n\tError occured loading dataset pickles {}'))
        print(colorful.red('\t{}\n'.format(e)))
        print(colorful.orange('\n\tCreating NN array for {}\n'.format(int(len(dftemp)))))

        # splits data before fiting
        train_size = (int(len(dftemp) * train_split))

        # drop close before min max scaling. dftempz is used for visualization
        dftempclose = dftemp.close
        dftemp.drop('close', axis=1, inplace=True)

        # separates label from feature dataset
        dftempy = dftemp.call
        dftemp.drop('call', axis=1, inplace=True)

        min_max_fit(dftemp[0:train_size], symbol, 'skl')
        dftemp.iloc[0:train_size] = min_max_load(dftemp.iloc[0:train_size], symbol, 'skl')
        dftemp.iloc[train_size:len(dftemp)] = min_max_load(
            dftemp.iloc[train_size:len(dftemp)], symbol, 'skl')

        dftemp = dftemp[items:]
        dftempy = dftempy[items:]
        dftempclose = dftempclose[items:]

        # converts sf to array
        x = dftemp.values
        y = dftempy.values
        close = dftempclose.values
        x = np.nan_to_num(x, copy=False)
        y = np.nan_to_num(y, copy=False)

        x_train, x_test, y_train, y_test = train_test_split(
            x, y, test_size=(1 - train_split), shuffle=False)

        np.save('data/training/{}/{}skxtr.npy'.format(symbol, symbol), x_train)
        np.save('data/training/{}/{}skxte.npy'.format(symbol, symbol), x_test)
        np.save('data/training/{}/{}skytr.npy'.format(symbol, symbol), y_train)
        np.save('data/training/{}/{}skyte.npy'.format(symbol, symbol), y_test)
        np.save('data/training/{}/{}skclose.npy'.format(symbol, symbol), close)

    print(colorful.green('\n\t' + symbol + ' Loading SKL Dataset Completed \n'))

    return x_train, x_test, y_train, y_test, close


def predict_nn_data(df, symbol):
    # copy to preserve main df integrity
    dftemp = df.copy()
    dftemp.drop('close', axis=1, inplace=True)

    # shrink df for speed
    dftemp = min_max_load(dftemp, symbol, 'nn')
    x = dftemp.values
    x = np.nan_to_num(x, copy=False)

    x1 = []
    temp = []
    count = 0

    for item in x:
        temp.append(item)
        if count > items - 1:
            x1.append(temp)
            temp = temp[1:]

        count += 1

    x2 = []
    for item in x1:
        item = item[::-1]
        x2.append(item)

    x2 = np.array(x2)

    print(colorful.green('\n\t' + symbol + ' Loading NN Prediction Dataset Completed \n'))

    return x2


def predict_skl_data(df, symbol):
    # copy to preserve main df integrity
    dftemp = df.copy()

    dftempy = dftemp.close
    dftemp.drop('close', axis=1, inplace=True)

    # shrink df for speed
    dftemp = min_max_load(dftemp, symbol, 'skl')

    # converts sf to array
    x = dftemp.values
    y = dftempy.values
    x = np.nan_to_num(x, copy=False)
    x = np.array(x)
    print(colorful.green('\n\t' + symbol + ' Loading SKL Prediction Dataset Completed \n'))

    return x, y


def model_testing_nn_data(df, symbol):
    # copy to preserve main df integrity
    dftemp = df.copy()
    dftemp.drop('close', axis=1, inplace=True)
    dftemp.drop('call', axis=1, inplace=True)
    dftemp = min_max_load(dftemp, symbol, 'nn')
    print('\n\tBacktesting NN list from {} to {}'.format(dftemp.index[0], dftemp.index[-1]))

    x = dftemp.values
    x = np.nan_to_num(x, copy=False)

    x1 = []
    temp = []
    count = 0

    for item in x:
        temp.append(item)
        if count > items - 1:
            x1.append(temp)
            temp = temp[1:]

        count += 1

    x2 = []
    for item in x1:
        item = item[::-1]
        x2.append(item)

    x2 = np.array(x2)

    print(colorful.orange('\n\t' + symbol + ' Loading NN Model Testing Dataset Completed \n'))

    return x2


def model_testing_skl_data(df, symbol):
    # copy to preserve main df integrity
    dftemp = df.copy()

    dftempy = dftemp.close
    dftemp.drop('close', axis=1, inplace=True)
    dftemp.drop('call', axis=1, inplace=True)

    # shrink df for speed
    dftemp = min_max_load(dftemp, symbol, 'skl')
    print('\n\tBacktesting SKL list from {} to {}'.format(dftemp.index[0], dftemp.index[-1]))

    # converts sf to array
    x = dftemp.values
    y = dftempy.values
    x = np.nan_to_num(x, copy=False)
    x = np.array(x)
    print(colorful.orange('\n\t' + symbol + ' Loading SKL Model Testing Dataset Completed \n'))

    return x, y


def model_testing_dataslice(df):
    import datetime
    start_date, start_month, start_hour, start_min, end_date, end_month, end_hour, end_min = 10, 1, 00, 00, 9, 3, 21, 00
    dftemp = df.copy()
    strtindx = '2018-{}-{} {}:{}:00'.format(start_month, start_date, start_hour, start_min)
    endindx = '2018-{}-{} {}:{}:00'.format(end_month, end_date, end_hour, end_min)
    strtindx = datetime.datetime.strptime(strtindx, '%Y-%m-%d %H:%M:%S')
    offset = datetime.timedelta(hours=5, minutes=30)
    strtindxnn = strtindx - offset
    dftempnn = dftemp[strtindxnn:endindx]
    dftempskl = dftemp[strtindx:endindx]

    return dftempnn, dftempskl


def create_folder(symbol):
    import os
    # stores symbols values
    directory = 'logs/{}'.format(symbol)
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
    except OSError:
        print(colorful.red('\n\tError: Creating directory. {}'.format(directory)))

    # stores previus version if retraining
    directory = 'logs/' + symbol + '/back'
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
            print(colorful.green('\n\tCreated directory. {}'.format(directory)))
    except OSError:
        print(colorful.red('\n\tError: Creating directory. {}'.format(directory)))

    # handles calgo predictions
    directory = "../calgo/{}.txt".format(symbol)
    try:
        if not os.path.isfile(directory):
            txt = open(directory, 'w')
            txt.write('0')
            txt.close()
            print(colorful.green('\n\tCreated directory. {}'.format(directory)))

    except OSError:
        print(colorful.red('\n\tError: Creating file. {}'.format(directory)))


def create_folder_pickle(symbol):
    # stores symbols values
    directory = 'data/training/{}'.format(symbol)
    try:
        if not os.path.exists(directory):
            os.makedirs(directory)
            print(colorful.green('\n\tCreated directory. {}'.format(directory)))
    except OSError:
        print(colorful.red('\n\tError: Creating directory. {}'.format(directory)))


def clean_tensorboard_folder():
    import os
    directory = './Graph'

    try:
        if os.listdir(directory):
            import shutil
            shutil.rmtree(directory)
            os.makedirs(directory)
            print(colorful.orange('\n\tClearing Tensorboard Graphs'))
    except OSError:
        print(colorful.red('\n\tError: Deleting Tensorboard directory.'))


def clean_model_testing(symbol):
    import os
    directory = './modeltest/{}-predlist.txt'.format(symbol)
    try:
        if os.path.exists(directory):
            os.remove(directory)
            print(colorful.orange('\n\tClearing old model predlist'))
        else:
            print(colorful.orange("\n\tNo predlist file exists"))
    except OSError:
        print(colorful.red('\n\tError: Clearing old model predlist.'))


if __name__ == "__main__":
    os.chdir('../')

    symbol = 'AUDJPY-Minute30'
    df = get_data(symbol, False)
    x_train, x_test, y_train, y_test = get_data_nn_class(df, symbol)
    print(x_train[0].shape)
