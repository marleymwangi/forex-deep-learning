import pandas as pd
import numpy as np
import colorful
from plotly import tools


def read_data(symbol, pred):
    # get_data()
    pred_dir = "data/predicting/"
    pred_dir = "data/training/"

    base_dir = pred_dir if pred else pred_dir

    file_loc = base_dir + "{}.csv".format(symbol)
    # ,usecols=['Date','Price'] ,index_col='Date'
    print(colorful.orange('\n\tReading {} files from csv\n'.format(symbol)))
    dfTemp = pd.read_csv(file_loc, parse_dates=True, na_values=['nan'])
    dfTemp.datetime = pd.to_datetime(dfTemp.datetime, format='%Y.%m.%d %H:%M:%S.%f')
    dfTemp.drop_duplicates()
    dfTemp = dfTemp.set_index('datetime')
    print(colorful.green('\n\tReading Complete\n'))

    return dfTemp


def read_comb_data(symbol):
    # get_data()
    comb_dir = "modeltest/{}-combpredlist.csv".format(symbol)
    # ,usecols=['Date','Price'] ,index_col='Date'
    print(colorful.orange('\n\tReading {} files from csv\n'.format(symbol)))
    dfTemp = pd.read_csv(comb_dir, na_values=['nan'])
    dfTemp.drop_duplicates()
    print(colorful.green('\n\tReading Complete\n'))

    return dfTemp


if __name__ == "__main__":
    import plotly as py
    import plotly.graph_objs as go

    import os
    os.chdir('../')

    df = read_comb_data('AUDJPY-Minute30')
