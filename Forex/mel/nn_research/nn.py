import pandas as pd
import numpy as np
import sys
import shutil
from featext import *
# from iq import *

# import libraries for neural net
from keras.models import Sequential
from keras.layers import Dense, Dropout
from keras.layers import LSTM, SimpleRNN
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras.optimizers import Adam
from keras.layers import advanced_activations

from keras.layers import Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D

from sklearn.metrics import classification_report


def build_model():
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    activationChoice = 'relu'

    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add(Conv2D(filters=32, kernel_size=(3, 3), activation=activationChoice, input_shape=(x_train.shape[1], x_train.shape[2], 1)))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Conv2D(64, (1, 3)))
    model.add(MaxPooling2D(pool_size=(1, 4)))
    model.add(Dropout(d))
    model.add(Flatten())
    model.add(Dense(128, activation=activationChoice))
    model.add(Dense(16, activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))

    optimizer = Adam(lr=0.010)
    model.compile(loss='categorical_crossentropy', optimizer='Adam', metrics=['accuracy'])  # para = ,metrics=['accuracy']
    model.summary()

    print('\n\tCreated RNN model\n')

    return model


def train_nn(nn):
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(filepath='logs/{}/{}/weights.hdf5'.format(symbol, nn), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0, write_graph=False, write_images=True)  # tensorboard --logdir=Graph
    model.fit(x_train, y_train, batch_size=64, epochs=2500, callbacks=[checkpointer, tbCallBack], validation_data=(x_test, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = model.predict(x_test)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print('\n\tRNN Precision Recall and F1\n')
    print(report)


def load_rnn():
    model.load_weights('logs/{}/weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print('\n\tLoaded {} RNN weights from file\n'.format(symbol))


def classwghtnn(y_train):
    # creates class weights using sklearn to deal with imbalanced data
    y_classes = []

    # Create a pd.series that represents the categorical class of each one-hot encoded row
    y_classes = np.argmax(y_train, axis=1)

    from sklearn.preprocessing import LabelEncoder
    # Instantiate the label encoder
    le = LabelEncoder()
    # Fit the label encoder to our label series
    le.fit(list(y_classes))
    # Create integer based labels Series
    y_integers = le.transform(list(y_classes))
    # Create dict of labels : integer representation
    labels_and_integers = dict(zip(y_classes, y_integers))

    from sklearn.utils.class_weight import compute_class_weight, compute_sample_weight
    class_weights = compute_class_weight('balanced', np.unique(y_integers), y_integers)
    sample_weights = compute_sample_weight('balanced', y_integers)

    class_weights_dict = dict(zip(le.transform(list(le.classes_)), class_weights))
    # print(class_weights_dict)

    return class_weights_dict


def visualize(x_test, y_test, price):
    # gets prediction and real values
    predicted_stock_pricernn, real_pred = predict_rnn(x_test, y_test)

    # combined ML algo evaluation
    report = classification_report(real_pred, predicted_stock_pricernn)
    print('\n\tCombined Precision Recall and F1\n')
    print(report)

    # plotly things
    trace1 = go.Scatter(y=real_pred, name=symbol + ' Real stock pred')
    trace2 = go.Scatter(y=price, name=symbol + ' Actual price')
    trace3 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')

    fig = tools.make_subplots(rows=3, cols=1, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 2, 1)
    fig.append_trace(trace3, 3, 1)

    py.offline.plot(fig, filename='plot.html')


def predict_rnn(x_test, y_test):
    # Making RNN Predictions and returns the predictions

    real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    return predicted_stock_price, real_pred


def get_index(array):
    # convert RNN results to 1d array
    lst = []

    for item in array:
        index = np.argmax(item)
        if index == 0:
            lst.append(1)
        elif index == 1:
            lst.append(0)
        elif index == 2:
            lst.append(-1)
    return lst


def rshape(unreshaped):
    # reshape for plotly compatibility
    unreshaped = np.array(unreshaped)
    reshaped = np.reshape(unreshaped, (unreshaped.shape[0]))

    return reshaped


symbol = 'test-EURUSD-Minute5'
# gets df with data and features
df = get_data(symbol, False)
# load RNN data from featext
x_train, x_test, y_train, y_test = get_data_nn_class(df, symbol)
_, _, _, _, price = get_data_skl(df, symbol)
cnn_shape = (x_train.shape[1], x_train.shape[2], 1)
x_train = x_train.reshape(x_train.shape[0], *cnn_shape)
x_test = x_test.reshape(x_test.shape[0], *cnn_shape)

model = build_model()
# train_rnn()
load_rnn()
v_slice = 600
visualize(x_test[-v_slice:], y_test[-v_slice:], price[-v_slice:])
