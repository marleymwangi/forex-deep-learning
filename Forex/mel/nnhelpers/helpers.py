import numpy as np


def classwghtnn(y_train):
    # creates class weights using sklearn to deal with imbalanced data
    y_classes = []

    # Create a pd.series that represents the categorical class of each one-hot encoded row
    y_classes = np.argmax(y_train, axis=1)

    from sklearn.preprocessing import LabelEncoder
    # Instantiate the label encoder
    le = LabelEncoder()

    # Fit the label encoder to our label series
    le.fit(list(y_classes))

    # Create integer based labels Series
    y_integers = le.transform(list(y_classes))

    # Create dict of labels : integer representation
    labels_and_integers = dict(zip(y_classes, y_integers))

    from sklearn.utils.class_weight import compute_class_weight, compute_sample_weight

    class_weights = compute_class_weight('balanced', np.unique(y_integers), y_integers)
    sample_weights = compute_sample_weight('balanced', y_integers)

    class_weights_dict = dict(zip(le.transform(list(le.classes_)), class_weights))

    # print(class_weights_dict)

    return class_weights_dict


def cnn_rshape(pred, x_train, x_test=0):
    # reshape for cnn input ... Requires 4dim
    cnn_shape = (x_train.shape[1], x_train.shape[2], 1)
    x_train_cnn = x_train.reshape(x_train.shape[0], *cnn_shape)
    if not pred:
        x_test_cnn = x_test.reshape(x_test.shape[0], *cnn_shape)
        return x_train_cnn, x_test_cnn
    elif pred:
        return x_train_cnn
