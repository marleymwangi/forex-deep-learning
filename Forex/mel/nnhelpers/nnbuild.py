from keras.models import Sequential
from keras.layers import Dense, Dropout, Flatten, Reshape
from keras.layers import LSTM, SimpleRNN, Conv2D, MaxPooling2D
from keras.optimizers import Adam
from keras.layers import advanced_activations

import colorful


def build_rnn_model(x_train, y_train):
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    nodes = 128
    activationChoice = 'relu'
    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add(LSTM(nodes, input_shape=(None, x_train.shape[2]), return_sequences=True))
    model.add(Dropout(d))
    model.add(LSTM(int(nodes * 0.5), return_sequences=True))
    model.add(Dropout(d))
    model.add(LSTM(nodes, return_sequences=False))
    model.add(Dropout(d))
    model.add(Dense(nodes, activation=activationChoice))  # ,kernel_initializer='uniform'
    model.add(Dense(int(nodes * 0.5), activation=activationChoice))
    model.add(Dense(int(nodes * 0.5), activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))
    # model.summary()

    optimizer = Adam(lr=0.001)
    model.compile(loss='categorical_crossentropy', optimizer='Adam',
                  metrics=['accuracy'])  # para = ,metrics=['accuracy']

    print(colorful.green('\n\tCreated RNN model\n'))

    return model


def build_cnnlstm_model(x_train, y_train):
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    activationChoice = 'relu'

    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add((Conv2D(filters=32, kernel_size=(3, 3), activation=activationChoice,
                      input_shape=(x_train.shape[1], x_train.shape[2], 1))))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Conv2D(64, (2, 3)))
    model.add(MaxPooling2D(pool_size=(3, 4)))
    model.add(Flatten())
    model.add(Reshape((1, -1)))
    model.add(LSTM(64, input_shape=(None, 256), return_sequences=False))
    model.add(Dropout(d))
    model.add(Dense(16, activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))

    optimizer = Adam(lr=0.010)
    model.compile(loss='categorical_crossentropy', optimizer='Adam',
                  metrics=['accuracy'])  # para = ,metrics=['accuracy']
    # model.summary()

    print(colorful.green('\n\tCreated CNN LSTM model\n'))

    return model


def build_cnn_model(x_train, y_train):
    # d  is for dropout rate refer to Bengio journal about drpoout rate can prevent overfitting
    # nodes is for how many nodes in each hidden layer
    # activationChoice is to set different activation function. Keras has softmax, relu, elu, tanh and others
    # returns built model
    d = 0.2
    activationChoice = 'relu'

    model = Sequential()
    # input shape = shape of data being fed eg 234,2,45...input id 45
    model.add(Conv2D(filters=32, kernel_size=(3, 3), activation=activationChoice,
                     input_shape=(x_train.shape[1], x_train.shape[2], 1)))
    model.add(MaxPooling2D(pool_size=(1, 2)))
    model.add(Conv2D(64, (2, 3)))
    model.add(MaxPooling2D(pool_size=(3, 4)))
    model.add(Dropout(d))
    model.add(Flatten())
    model.add(Dense(128, activation=activationChoice))
    model.add(Dense(16, activation=activationChoice))
    model.add(Dense(y_train.shape[1], activation='softmax'))

    optimizer = Adam(lr=0.010)
    model.compile(loss='categorical_crossentropy', optimizer='Adam',
                  metrics=['accuracy'])  # para = ,metrics=['accuracy']
    # model.summary()

    print(colorful.green('\n\tCreated CNN model\n'))

    return model
