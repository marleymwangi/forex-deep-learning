import colorful
from keras.optimizers import Adam


def load_rnn(rnn_model, symbol):
    rnn_model.load_weights('logs/{}/rnn_weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    rnn_model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print(colorful.green('\n\tLoaded {} RNN weights from file\n'.format(symbol)))


def load_cnnlstm(cnnlstm_model, symbol):
    cnnlstm_model.load_weights('logs/{}/cnnlstm_weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    cnnlstm_model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print(colorful.green('\n\tLoaded {} CNNLSTM weights from file\n'.format(symbol)))


def load_cnn(cnn_model, symbol):
    cnn_model.load_weights('logs/{}/cnn_weights.hdf5'.format(symbol))
    optimizer = Adam(lr=0.001)
    # Compile model (required to make predictions) from saved weights
    cnn_model.compile(optimizer=optimizer, loss='categorical_crossentropy')
    print(colorful.green('\n\tLoaded {} CNN weights from file\n'.format(symbol)))
