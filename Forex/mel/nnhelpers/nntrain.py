import numpy as np
import colorful

from keras.callbacks import ModelCheckpoint, TensorBoard

from sklearn.metrics import classification_report

from file_man.featext import clean_tensorboard_folder
from .helpers import classwghtnn


def train_rnn(x_train, x_test, y_train, y_test, symbol, rnn_model):
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(
        filepath='logs/{}/rnn_weights.hdf5'.format(symbol), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0,
                             write_graph=False, write_images=True)  # tensorboard --logdir=Graph
    rnn_model.fit(x_train, y_train, batch_size=64, epochs=60, callbacks=[
                  checkpointer, tbCallBack], validation_data=(x_test, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = rnn_model.predict(x_test)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print(colorful.orange('\n\tRNN Precision Recall and F1\n'))
    print(report)


def train_cnnlstm(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnnlstm_model):
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(
        filepath='logs/{}/cnnlstm_weights.hdf5'.format(symbol), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0,
                             write_graph=False, write_images=True)
    cnnlstm_model.fit(x_train_cnn, y_train, batch_size=1024, epochs=500, callbacks=[
                      checkpointer, tbCallBack], validation_data=(x_test_cnn, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = cnnlstm_model.predict(x_test_cnn)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print(colorful.orange('\n\tCNNLSTM Precision Recall and F1\n'))
    print(colorful.green(report))


def train_cnn(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnn_model):
    # gets class weight to compensate for skewed data
    class_weights = classwghtnn(y_train)
    # Fitting the Recurrent Neural Network to the Training set
    checkpointer = ModelCheckpoint(
        filepath='logs/{}/cnn_weights.hdf5'.format(symbol), verbose=1, monitor='val_acc', save_best_only=True)
    clean_tensorboard_folder()
    tbCallBack = TensorBoard(log_dir='./Graph', histogram_freq=0,
                             write_graph=False, write_images=True)
    cnn_model.fit(x_train_cnn, y_train, batch_size=1024, epochs=500, callbacks=[
                  checkpointer, tbCallBack], validation_data=(x_test_cnn, y_test), class_weight=class_weights)

    # Model evaluation using sklearn
    pred = cnn_model.predict(x_test_cnn)
    predicted = np.argmax(pred, axis=1)
    report = classification_report(np.argmax(y_test, axis=1), predicted)
    print(colorful.orange('\n\tCNN Precision Recall and F1\n'))
    print(colorful.green(report))
