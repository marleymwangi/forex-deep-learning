import numpy as np
import colorful


def rshape(unreshaped):
    # reshape for plotly compatibility
    unreshaped = np.array(unreshaped)
    reshaped = np.reshape(unreshaped, (unreshaped.shape[0]))

    return reshaped


def get_index(array):
    # convert RNN results to 1d array
    lst = []

    for item in array:
        index = np.argmax(item)
        if index == 0:
            lst.append(1)
        elif index == 1:
            lst.append(0)
        elif index == 2:
            lst.append(-1)
    return lst


def colored(num):
    if num == 1:
        num = colorful.green(num)
    elif num == -1:
        num = colorful.red(num)
    else:
        num = colorful.yellow(num)
    return num
