import plotly.graph_objs as go
import plotly.tools as tools
import plotly as py

from sklearn.metrics import classification_report
from .predict import *


def visualize(x_test_cnn, x_test, y_test, x_test_skl, price, pred_bool, rnn_model, cnn_model, cnnlstm_model, svm, lreg, lda, symbol):
    # gets prediction and real values
    predicted_stock_pricernn, real_pred = predict_rnn(x_test, y_test, pred_bool, rnn_model)
    # gets prediction and real values
    predicted_stock_pricecnnlstm, _ = predict_cnnlstm(x_test_cnn, y_test, pred_bool, cnnlstm_model)
    # gets prediction and real values
    predicted_stock_pricecnn, _ = predict_cnn(x_test_cnn, y_test, pred_bool, cnn_model)

    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(x_test_skl, svm)
    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(x_test_skl, lreg)
    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(x_test_skl, lda)

    # combines all ml predictions into one thing
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnnlstm = predicted_stock_pricecnnlstm[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnnlstm == next_pred_cnn == next_pred_svm == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0
        combined_pred.append(position)

        '''
    outputFileName = "modeltest/{}-combpredlist.csv".format(symbol)
    for i in range(len(combined_pred)):
        with open(outputFileName, "a") as text_file:
            text_file.write("{},{},{},{},{},{},{},{},{}\n".format(predicted_stock_pricernn[i], predicted_stock_pricecnnlstm[i], predicted_stock_pricecnn[
                            i], predicted_stock_pricesvm[i], predicted_stock_pricelreg[i], predicted_stock_pricelda[i], combined_pred[i], real_pred[i], price[i]))
        '''
    # combined ML algo evaluation
    report = classification_report(real_pred, combined_pred)
    print('\n\tCombined Precision Recall and F1\n')
    print(report)

    # plotly things
    trace1 = go.Scatter(y=real_pred, name=symbol + ' Real stock pred')
    trace2 = go.Scatter(y=price, name=symbol + ' Actual price')
    trace3 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
    trace4 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
    trace5 = go.Scatter(y=predicted_stock_pricecnnlstm, name=symbol + ' CNNLSTM predicted price')
    trace6 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
    trace7 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
    trace8 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
    trace9 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

    fig = tools.make_subplots(rows=9, cols=1, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 2, 1)
    fig.append_trace(trace3, 3, 1)
    fig.append_trace(trace4, 4, 1)
    fig.append_trace(trace5, 5, 1)
    fig.append_trace(trace6, 6, 1)
    fig.append_trace(trace7, 7, 1)
    fig.append_trace(trace8, 8, 1)
    fig.append_trace(trace9, 9, 1)

    py.offline.plot(fig, filename='plot.html')


def prediction(pred_nn, pred_cnn, pred_skl, pred_price, visual_pred, pred_bool, rnn_model, cnn_model, cnnlstm_model, svm, lreg, lda, symbol):
    y_test = [1, 0, 1]
    # gets prediction and real values
    predicted_stock_pricernn = predict_rnn(pred_nn, y_test, pred_bool, rnn_model)
    # gets prediction and real values
    predicted_stock_pricecnnlstm = predict_cnnlstm(pred_cnn, y_test, pred_bool, cnnlstm_model)
    # gets prediction and real values
    predicted_stock_pricecnn = predict_cnn(pred_cnn, y_test, pred_bool, cnn_model)

    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(pred_skl, svm)
    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(pred_skl, lreg)
    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(pred_skl, lda)

    # combines all ml predictions into one thing
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnnlstm = predicted_stock_pricecnnlstm[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnnlstm == next_pred_cnn == next_pred_svm == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0
        combined_pred.append(position)

    outputFileName = "../calgo/{}.txt".format(symbol)
    with open(outputFileName, "w") as text_file:
        text_file.write("{0}".format(combined_pred[-1]))

    print(len(combined_pred))

    print('\t{} RNN: {} ,CNNLSTM: {},CNN: {} ,SVM: {} ,LREG: {} ,LDA: {}. Conclusion is {}'
          .format(
              symbol,
              colored(predicted_stock_pricernn[-1]),
              colored(predicted_stock_pricecnnlstm[-1]),
              colored(predicted_stock_pricecnn[-1]),
              colored(predicted_stock_pricesvm[-1]),
              colored(predicted_stock_pricelreg[-1]),
              colored(predicted_stock_pricelda[-1]),
              colored(combined_pred[-1])))

    # plotly things
    if visual_pred:
        # plotly things
        trace1 = go.Scatter(y=pred_price, name=symbol + ' Actual price')
        trace2 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
        trace3 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
        trace4 = go.Scatter(y=predicted_stock_pricecnnlstm,
                            name=symbol + ' CNNLSTM predicted price')
        trace5 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
        trace6 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
        trace7 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
        trace8 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

        fig = tools.make_subplots(rows=8, cols=1, shared_xaxes=True, shared_yaxes=True)
        fig.append_trace(trace1, 1, 1)
        fig.append_trace(trace2, 2, 1)
        fig.append_trace(trace3, 3, 1)
        fig.append_trace(trace4, 4, 1)
        fig.append_trace(trace5, 5, 1)
        fig.append_trace(trace6, 6, 1)
        fig.append_trace(trace7, 7, 1)
        fig.append_trace(trace8, 8, 1)

        py.offline.plot(fig, filename='plot.html')


def model_testing(df, symbol, y_test, rnn_model, cnn_model, cnnlstm_model, svm, lreg, lda):
    from file_man.featext import model_testing_dataslice, model_testing_nn_data, model_testing_skl_data, clean_model_testing
    from nnhelpers.helpers import cnn_rshape

    import sys
    sys.path.append("..")  # Adds higher directory to python modules path.

    print('\n\tCreating model_testing pred list')
    modeldf, _ = model_testing_dataslice(df)
    pred_nn = model_testing_nn_data(modeldf, symbol)
    pred_cnn = cnn_rshape(True, pred_nn)

    _, modeldf = model_testing_dataslice(df)
    pred_skl, pred_price = model_testing_skl_data(modeldf, symbol)

    # gets prediction and real values
    predicted_stock_pricernn = predict_rnn(pred_nn, y_test, True, rnn_model)
    # gets prediction and real values
    predicted_stock_pricecnnlstm = predict_cnnlstm(pred_cnn, y_test, True, cnnlstm_model)
    # gets prediction and real values
    predicted_stock_pricecnn = predict_cnn(pred_cnn, y_test, True, cnn_model)

    # Getting the prediction from svm
    predicted_stock_pricesvm = predict_skl(pred_skl, svm)
    print(len(predicted_stock_pricesvm))
    # Getting the prediction from lreg
    predicted_stock_pricelreg = predict_skl(pred_skl, lreg)
    # Getting the prediction from lda
    predicted_stock_pricelda = predict_skl(pred_skl, lda)

    # selecting last predictions from all machines
    combined_pred = []
    for i in range(len(predicted_stock_pricernn)):
        next_pred_rnn = predicted_stock_pricernn[i]
        next_pred_cnnlstm = predicted_stock_pricecnnlstm[i]
        next_pred_cnn = predicted_stock_pricecnn[i]
        next_pred_svm = predicted_stock_pricesvm[i]
        next_pred_lreg = predicted_stock_pricelreg[i]
        next_pred_lda = predicted_stock_pricelda[i]

        # if all machines agree then set position
        if (next_pred_rnn == next_pred_cnnlstm == next_pred_cnn == next_pred_svm == next_pred_lreg == next_pred_lda):
            position = next_pred_rnn
        else:
            position = 0

        combined_pred.append(position)

    # plotly things
    trace1 = go.Scatter(y=pred_price, name=symbol + ' Actual price')
    trace2 = go.Scatter(y=combined_pred, name=symbol + ' Combined Pred')
    trace3 = go.Scatter(y=predicted_stock_pricernn, name=symbol + ' RNN predicted price')
    trace4 = go.Scatter(y=predicted_stock_pricecnnlstm, name=symbol + ' CNNLSTM predicted price')
    trace5 = go.Scatter(y=predicted_stock_pricecnn, name=symbol + ' CNN predicted price')
    trace6 = go.Scatter(y=predicted_stock_pricesvm, name=symbol + ' SVM predicted price')
    trace7 = go.Scatter(y=predicted_stock_pricelreg, name=symbol + ' LREG predicted price')
    trace8 = go.Scatter(y=predicted_stock_pricelda, name=symbol + ' lda predicted price')

    fig = tools.make_subplots(rows=8, cols=1, shared_xaxes=True, shared_yaxes=True)
    fig.append_trace(trace1, 1, 1)
    fig.append_trace(trace2, 2, 1)
    fig.append_trace(trace3, 3, 1)
    fig.append_trace(trace4, 4, 1)
    fig.append_trace(trace5, 5, 1)
    fig.append_trace(trace6, 6, 1)
    fig.append_trace(trace7, 7, 1)
    fig.append_trace(trace8, 8, 1)

    py.offline.plot(fig, filename='plot.html')

    print('\n\tSaving model_testing pred list')
    clean_model_testing(symbol)
    print('\n\tBuys {} and Sells {}. {} trades in total'.format(combined_pred.count(1),
                                                                combined_pred.count(-1), (combined_pred.count(1) + combined_pred.count(-1))))
    outputFileName = "modeltest/{}-predlist.txt".format(symbol)
    for i in range(len(combined_pred)):
        with open(outputFileName, "a") as text_file:
            text_file.write("{}\n".format(combined_pred[i]))
    print('\n\tSaved model_testing pred list')
