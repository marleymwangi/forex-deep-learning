from .helpers import *


def predict_rnn(x_test, y_test, pred_bool, rnn_model):
    # Making RNN Predictions and returns the predictions

    # real values are needed for training visualisation but not prediction
    if not pred_bool:
        real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = rnn_model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    # since real values arent needed in pred
    if not pred_bool:
        return predicted_stock_price, real_pred
    elif pred_bool:
        return predicted_stock_price


def predict_cnnlstm(x_test, y_test, pred_bool, cnnlstm_model):
    # Making RNN Predictions and returns the predictions

    # real values are needed for training visualisation but not prediction
    if not pred_bool:
        real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = cnnlstm_model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    # since real values arent needed in pred
    if not pred_bool:
        return predicted_stock_price, real_pred
    elif pred_bool:
        return predicted_stock_price


def predict_cnn(x_test, y_test, pred_bool, cnn_model):
    # Making RNN Predictions and returns the predictions

    # real values are needed for training visualisation but not prediction
    if not pred_bool:
        real_pred = get_index(y_test)

    # Getting the prediction from RNN
    predicted_stock_pricetemp = cnn_model.predict(x_test)
    # convert RNN results to 1d array
    predicted_stock_price = get_index(predicted_stock_pricetemp)
    # reshape for plotly compatibility
    predicted_stock_price = rshape(predicted_stock_price)

    # since real values arent needed in pred
    if not pred_bool:
        return predicted_stock_price, real_pred
    elif pred_bool:
        return predicted_stock_price


def predict_skl(x_test_skl, skl):
    # Getting the prediction from skl model passed
    predicted_stock_priceskl = skl.predict(x_test_skl)
    # reshape for plotly compatibility
    predicted_stock_priceskl = rshape(predicted_stock_priceskl)

    return predicted_stock_priceskl
