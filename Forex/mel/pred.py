import pandas as pd
import numpy as np
import sys
import shutil
import colorful

# import functions:
# data handling
from file_man.featext import *
# reshape data for cnn
from nnhelpers.helpers import cnn_rshape
# building and training neural nets
from nnhelpers.nnbuild import *
# training nn
from nnhelpers.nntrain import *
# load nn
from nnhelpers.nnload import *
# train skl
from sklhelpers.skltrain import *
# load skl
from sklhelpers.sklload import *
# plot results
from plothelpers.plotters import *


# from iq import *
from sklearn.metrics import classification_report


retrain_text = """
\tRetrain Model code:\t[Model] [ Save ] [ Retrain ]:
\t\t\t1: Yes\t 1: Yes
\t\t\t2: No\t 2: No
\t\t\t3: blank
\t\t\tModel codes
\t1:RNN 2:CNNLSTM 3:CNN 4:DEEP_ALL 5:SVM 6:LREG 7:LDA 8:SKL 9:ALL 0:NOTHING

\tRetrain Model:"""


# checks if correct number of arguments are passed ..if not prompt
if len(sys.argv) != 7:
    print(colorful.red('Error Passing arguments to Main Prediction file'))
    exit()

train_int, visual, pred_bool, retrain_bool, model_testing_bool, symbol_par = int(sys.argv[1]), int(
    sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5]), str(sys.argv[6])

visual = True if visual == 1 else False
pred_bool = True if pred_bool == 1 else False
retrain_bool = True if retrain_bool == 1 else False
model_testing_bool = True if model_testing_bool == 1 else False
visual_pred = True if pred_bool and visual else False
if pred_bool and visual:
    visual = False


def train_deep_all():
    # helper function to help with train function below
    train_rnn(x_train, x_test, y_train, y_test, symbol, rnn_model)
    train_cnnlstm(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnnlstm_model)
    train_cnn(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnn_model)


def train_rnn_only():
    train_rnn(x_train, x_test, y_train, y_test, symbol, rnn_model)


def train_cnnlstm_only():
    train_cnnlstm(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnnlstm_model)


def train_cnn_only():
    train_cnn(x_train_cnn, x_test_cnn, y_train, y_test, symbol, cnn_model)


def train_all():
    # helper function to help with train function below
    train_deep_all()
    train_skl_only()


def train_svm_only():
    train_svm(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)


def train_lreg_only():
    train_lreg(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)


def train_lda_only():
    train_lda(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)


def train_skl_only():
    train_svm(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)
    train_lreg(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)
    train_lda(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol)


def train(option):
    switcher = {
        1: train_rnn_only,
        2: train_cnnlstm_only,
        3: train_cnn_only,
        4: train_deep_all,
        5: train_svm_only,
        6: train_lreg_only,
        7: train_lda_only,
        8: train_skl_only,
        9: train_all}

    # Get the function from switcher dictionary
    func = switcher.get(option, lambda: None)
    # Execute the function
    func()

# loads all saved weights for use


def load():
    load_rnn(rnn_model, symbol)
    load_cnnlstm(cnnlstm_model, symbol)
    load_cnn(cnn_model, symbol)
    svm = load_svm(symbol)
    lreg = load_lreg(symbol)
    lda = load_lda(symbol)
    return svm, lreg, lda


def save_retrain(symbol, train_int):
    models = {4: 'sgd', 5: 'svm', 6: 'lreg', 7: 'lda'}
    model = models.setdefault(train_int, 'sgd')
    shutil.copy('logs/{}/{}.pkl'.format(symbol, model),
                'logs/{}/back/{}.pkl'.format(symbol, models[train_int]))
    print('\n\t{} {} weights saved in backup\n'.format(symbol, model))


def cpy_back(symbol):
    shutil.copy('logs/{}/back/sgd.pkl'.format(symbol), 'logs/{}/sgd.pkl'.format(symbol))
    print('\n\t{} weights loaded from backup\n'.format(symbol))


symbol = '{}-Minute30'.format(symbol_par)
# gets df with data and features
df = get_data(symbol, False)
# load RNN data from featext
x_train, x_test, y_train, y_test = get_data_nn_class(df, symbol)
# load RNN data from featext
x_train_skl, x_test_skl, y_train_skl, y_test_skl, price = get_data_skl(df, symbol)

x_train_cnn, x_test_cnn = cnn_rshape(False, x_train, x_test)

rnn_model = build_rnn_model(x_train, y_train)
cnnlstm_model = build_cnnlstm_model(x_train, y_train)
cnn_model = build_cnn_model(x_train, y_train)

# Loop to retrain desired models
while True:
    train(train_int)
    svm, lreg, lda = load()  # run model testing # sgd,

    # Loop to retrain desired models
    if visual:
        v_slice = 5000
        visualize(x_test_cnn[-v_slice:], x_test[-v_slice:],
                  y_test[-v_slice:], x_test_skl[-v_slice:], price[-v_slice:], pred_bool, rnn_model, cnn_model, cnnlstm_model, svm, lreg, lda, symbol)
        # play sound once training and visualization is done
        import winsound
        duration, freq = 400, 440  # duration - millisecond,frequency in Hz
        winsound.Beep(freq, duration)

    # exit loop is user doesn't want to retrain
    if not retrain_bool:
        break
    elif retrain_bool:
        # get user input for trainnum, save and retrain
        retrain_prompt = input(retrain_text)

        # save previous weights before retraining
        # saves sgd on first run since is changes alot
        save = int(str(retrain_prompt)[1])
        # save model in back if yes
        save_bool = True if save == 1 else False
        if save_bool:
            save_retrain(symbol, train_int)

        # changes Train int for second run
        train_int = int(str(retrain_prompt)[0])

        # End loop if retrain is no
        retrain = int(str(retrain_prompt)[2])
        retrain_bool = True if retrain == 1 else False
        if retrain_bool:
            print('\tRetraining model {}'.format(train_int))
        if train_int == 0 and retrain_bool == False and save_bool == False:
            cpy_back(symbol)
        if not retrain_bool:
            print('\n\tExiting {} retraining'.format(symbol))
            break
# run model testing
if model_testing_bool:
    modeldf = get_data(symbol, False)
    model_testing(modeldf, symbol, y_test, rnn_model, cnn_model,
                  cnnlstm_model, svm, lreg, lda)


if pred_bool:
    import csv
    import time

    try:
        print(colorful.orange("\n\nRunning {} min Pred:\nTime ".format(symbol[-2:])))
        print(colorful.orange(time.strftime("%H:%M:%S")))
        print("\n")
        df = get_data(symbol, True)
        pred_nn = predict_nn_data(df, symbol)
        pred_cnn = cnn_rshape(True, pred_nn)
        pred_skl, pred_price = predict_skl_data(df, symbol)
        v_slice = 600
        prediction(pred_nn[-v_slice:], pred_cnn[-v_slice:], pred_skl[-v_slice:], pred_price[-v_slice:],
                   visual_pred, pred_bool, rnn_model, cnn_model, cnnlstm_model, svm, lreg, lda, symbol)
        print(colorful.green("\n\nCompleted!\nTime "))
        print(colorful.green(time.strftime("%H:%M:%S")))

    except KeyboardInterrupt:
        print('\n\nManual break by user \n\n')
