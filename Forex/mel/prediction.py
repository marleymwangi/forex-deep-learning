import sys  # for handling parameters
import os
import colorful

# get training ploting and predicting parameters
usage = """
\n\tError001: 3 Program arguments expected

\tExpected: python prediction.py [train] [visualize] [predict] [ Retrain ] [Model Testing]
\t  [  Train  ]\t\t[ Visualize ]\t[ Predict ]\t[ Retrain ]\t[Model Testing]
\t 1: train_rnn,\t\t1:yes\t\t1:yes\t\t1:yes\t\t1:yes
\t 2: train_cnnlstm,\t0:no\t\t0:no\t\t0:no\t\t0:no
\t 3: train_cnn,
\t 4: train_deep_all,
\t 5: train_svm,
\t 6: train_lreg,
\t 7: train_lda,
\t 8: train_skl,
\t 9: train_all
\t 0: train_nothing
"""

# checks if correct number of arguments are passed ..if not prompt
if len(sys.argv) != 6:
    print(colorful.orange(usage))
    exit()
else:
    print(colorful.green("RUNNING"))

train_int, visual, pred_bool, retrain_bool, model_testing_bool = int(sys.argv[1]), int(
    sys.argv[2]), int(sys.argv[3]), int(sys.argv[4]), int(sys.argv[5])

# symbols = ['AUDJPY','AUDUSD','EURCHF','EURGBP','EURJPY','EURUSD','GBPJPY','GBPUSD', 'USDCAD','USDCHF', 'USDJPY','#Euro50','#Germany30','#Japan225','#UK100','#USNDAQ100','#USSPX500']
# list of symbols to be used for training
if not pred_bool:
    symbols = ['AUDJPY']

# list of symbols to be used for prediction
if pred_bool:
    symbols = ['AUDUSD']

# Loop through training symbols list and call python pred.py file: Calling as a separate file helps avoid -1 syndrome
if not pred_bool:
    for symbol_ in symbols:
        symbol = '{}'.format(symbol_)
        os.system('python pred.py {} {} {} {} {} {}'.format(
            train_int, visual, pred_bool, retrain_bool, model_testing_bool, symbol))

# Loop through prediction symbols list and call python pred.py file: Calling as a separate file helps avoid -1 syndrome
if pred_bool:
    import datetime  # for getting current time
    import time  # for sleep

    '''
    Infinite loop that calls pred every 5 min
    '''
    try:
        while True:
            if datetime.datetime.now().minute % int(1) == 0:
                global start_min
                start_min = datetime.datetime.now().minute
                for symbol_ in symbols:
                    symbol = '{}'.format(symbol_)
                    os.system('python pred.py {} {} {} {} {} {}'.format(
                        train_int, visual, pred_bool, retrain_bool, model_testing_bool, symbol))

                if datetime.datetime.now().minute == start_min:
                    time.sleep(60 - datetime.datetime.now().second)

    except KeyboardInterrupt:
        print(colorful.orange('\n\nManual break by user \n\n'))
