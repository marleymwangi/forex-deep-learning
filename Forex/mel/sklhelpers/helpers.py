import numpy as np


def classwghtskl(y_train_skl):
    from sklearn.utils.class_weight import compute_class_weight

    class_weight_list = compute_class_weight('balanced', np.unique(y_train_skl), y_train_skl)
    class_weights_dict = dict(zip(np.unique(y_train_skl), class_weight_list))

    # print(class_weights_dict)

    return class_weights_dict
