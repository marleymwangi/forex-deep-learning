import colorful
from sklearn.externals import joblib


def load_svm(symbol):
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/svm.pkl'.format(symbol))
    print(colorful.green('\n\tLoaded {} Linear SVC weights from file\n'.format(symbol)))
    return lda


def load_lreg(symbol):
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/lreg.pkl'.format(symbol))
    print(colorful.green('\n\tLoaded {} Logistic Regression weights from file\n'.format(symbol)))
    return lda


def load_sgd(symbol):
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/sgd.pkl'.format(symbol))
    print(colorful.green('\n\tLoaded {} SGD Classifier weights from file\n'.format(symbol)))
    return lda


def load_lda(symbol):
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/lda.pkl'.format(symbol))
    print(colorful.green('\n\tLoaded {} LinearDiscriminantAnalysis weights from file\n'.format(symbol)))
    return lda


def load_qda(symbol):
    # Get model (required to make predictions) from saved weights
    lda = joblib.load('logs/{}/qda.pkl'.format(symbol))
    print(colorful.green('\n\tLoaded {} QuadraticDiscriminantAnalysis weights from file\n'.format(symbol)))
    return lda
