import colorful

# import libraries for ML
from sklearn import linear_model
from sklearn.svm import LinearSVC
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from sklearn.externals import joblib
from sklearn.metrics import classification_report

from .helpers import classwghtskl


def train_svm(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol):
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Linear SVC classifier
    clf = LinearSVC(class_weight=class_weights)  # , loss='hinge'
    print(colorful.green('\n\tCreated {} Linear SVC model\n'.format(symbol)))
    print(colorful.orange('\n\tTraining {} Linear SVC\n'.format(symbol)))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print(colorful.green('\n\t {} Linear SVC accuracy is {}\n'.format(symbol, accuracy)))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print(colorful.orange('\n\tLinear SVC Precision Recall and F1\n'))
    print(colorful.green(report))
    '''
    print(y_test_skl[0:10])
    print(predicted[0:10])
    print(clf.predict_proba(x_test_skl[0:10]))
    '''
    # save the trained weight values
    joblib.dump(clf, 'logs/{}/svm.pkl'.format(symbol))
    print(colorful.green('\n\t{} Linear SVC weights saved\n'.format(symbol)))


def train_lreg(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol):
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Logistic Regression classifier
    clf = linear_model.LogisticRegression(class_weight=class_weights)
    print(colorful.green('\n\tCreated {} Logistic Regression model\n'.format(symbol)))
    print(colorful.orange('\n\tTraining {} Logistic Regression\n'.format(symbol)))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print(colorful.green('\n\t{} Logistic Regression accuracy is {}\n'.format(symbol, accuracy)))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print(colorful.orange('\n\tLogistic Regression Precision Recall and F1\n'))
    print(colorful.green(report))

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/lreg.pkl'.format(symbol))
    print(colorful.green('\n\t{} Logistic Regression weights saved\n'.format(symbol)))


def train_lda(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol):
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Linear Discriminant Analysis classifier
    clf = LinearDiscriminantAnalysis()
    print(colorful.green('\n\tCreated {} LinearDiscriminantAnalysis model\n'.format(symbol)))
    print(colorful.orange('\n\tTraining {} LinearDiscriminantAnalysis\n'.format(symbol)))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print(colorful.green('\n\t{} LinearDiscriminantAnalysis accuracy is {}\n'.format(symbol, accuracy)))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print(colorful.orange('\n\tLinearDiscriminantAnalysis Precision Recall and F1\n'))
    print(colorful.green(report))

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/lda.pkl'.format(symbol))
    print(colorful.green('\n\t{} LinearDiscriminantAnalysis weights saved\n'.format(symbol)))


def train_qda(x_train_skl, x_test_skl, y_train_skl, y_test_skl, symbol):
    # gets class weight to compensate for skewed data
    class_weights = classwghtskl(y_train_skl)
    # training Quadratic Discriminant Analysis classifier
    clf = QuadraticDiscriminantAnalysis()
    print(colorful.green('\n\tCreated {} QuadraticDiscriminantAnalysis model\n'.format(symbol)))
    print(colorful.orange('\n\tTraining {} QuadraticDiscriminantAnalysis\n'.format(symbol)))
    clf.fit(x_train_skl, y_train_skl)

    # Model evaluation using sklearn
    accuracy = clf.score(x_test_skl, y_test_skl)
    print(colorful.green('\n\t{} QuadraticDiscriminantAnalysis accuracy is {}\n'.format(symbol, accuracy)))

    predicted = clf.predict(x_test_skl)
    report = classification_report(y_test_skl, predicted)
    print(colorful.orange('\n\tQuadraticDiscriminantAnalysis Precision Recall and F1\n'))
    print(colorful.green(report))

    # save the trained weight values
    joblib.dump(clf, 'logs/{}/qda.pkl'.format(symbol))
    print(colorful.green('\n\t{} QuadraticDiscriminantAnalysis weights saved\n'.format(symbol)))
