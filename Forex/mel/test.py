import pandas as pd
import numpy as np
import colorful
from scipy.signal import argrelmax, argrelmin, find_peaks
import matplotlib.pyplot as plt


def classify(df):

    dftemp = df.copy()
    ordernum = 5
    price = dftemp.close

    print(colorful.orange('\n\tStarting Classification of {} samples\n'.format(int(len(dftemp)))))

    print(colorful.green('\n\tClassification Complete\n'))
    calls = np.zeros(len(dftemp))

    [sellind, _] = find_peaks(price, prominence=0.2)
    invertedprice = max(price) - price
    [buyind, _] = find_peaks(invertedprice, prominence=0.2)

    for sell in sellind:
        calls[sell] = -1

    for buy in buyind:
        calls[buy] = 1

    dftemp['call'] = calls

    return dftemp.call


# imprt df
df = pd.read_csv('data/training/AUDjpy-Minute15.csv', parse_dates=True, na_values=['nan'], usecols=['datetime', 'close', 'ema1'])

df.datetime = pd.to_datetime(df.datetime, format='%Y.%m.%d %H:%M:%S.%f')

df.drop_duplicates()

df = df.set_index(df.datetime)

#df = df.iloc[-3000:]
df['call'] = classify(df)

df = df[['close', 'call']]

call = df.call.values
price = df.close
buyl = []
selll = []


for i in range(0, len(price)):
    if call[i] == 1:
        buyl.append(i)
    if call[i] == -1:
        selll.append(i)

# print(buyl)
# print(selll)

buyp = price.values[buyl]
sellp = price.values[selll]

plt.plot(price.values)
plt.scatter(buyl, buyp, c='g')
plt.scatter(selll, sellp, c='r')
plt.show()
