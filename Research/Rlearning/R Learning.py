from __future__ import print_function
import os
import sys
import time
import datetime
import json
import random
import numpy as np
import pandas as pd
from sklearn import preprocessing
from keras.models import Sequential
from keras.layers.core import Dense, Activation
from keras.optimizers import SGD, Adam, RMSprop
from keras.layers.advanced_activations import PReLU
import matplotlib.pyplot as plt
import matplotlib.animation as animation


plt.rcParams['animation.ffmpeg_path'] = 'ffmpeg\\bin\\ffmpeg.exe'
FFwriter = animation.FFMpegWriter()

min_max_scaler = preprocessing.MinMaxScaler(feature_range=(0, 1))

# User defined variables
days = 1
pip_value = 0.0001
pip_dollar_value = 1
commission = 1.5
leverage = int(pip_dollar_value / pip_value)
commission = commission * pip_value * leverage

account_start = 1000
max_loss_allowed = 1000
training_target = 1100


def read_data():
    # get_data()
    file_loc = "data/gbpusd.csv"
    dfTemp = pd.read_csv(file_loc, parse_dates=True, na_values=['nan'])
    dfTemp.drop_duplicates()
    dfTemp.drop('datetime', axis=1, inplace=True)
    # dfTemp.datetime = pd.to_datetime(dfTemp.datetime, format='%Y.%m.%d %H:%M:%S.%f')
    # dfTemp = dfTemp.set_index('datetime')

    return dfTemp


def min_max(df):
    # copy df to preserve integrity
    dftemp = df.copy()
    # using sklearn min max scaler for normalization and fit and save the norm values for later use
    scaler = min_max_scaler.fit(dftemp[dftemp.columns])
    dftemp[dftemp.columns] = scaler.transform(dftemp[dftemp.columns])
    return dftemp


predexitem = 5
df = read_data()
df = df[predexitem:(1440 * days)]
dfog = df.copy()
dfog['calls'] = 0

# creates a multidimensional tensor allexamples by items+1 by features
feat1 = df.values
count = 0
temp = []
feat = []

for item in feat1:
    temp.append(item)
    if count >= predexitem:
        feat.append(temp)
        temp = temp[1:]
    count += 1

feat = np.array(feat)

# scales prices using close
df.open = (df.close - df.open)
df.high = (df.close - df.high)
df.low = (df.close - df.low)
df.wclose = (df.close - df.wclose)

df.ema1 = (df.close - df.ema1)
df.ema2 = (df.close - df.ema2)
df.ema3 = (df.close - df.ema3)
df.ema4 = (df.close - df.ema4)

df.hband = (df.close - df.hband)
df.mband = (df.close - df.mband)
df.lband = (df.close - df.lband)

#df.drop('close', axis=1, inplace=True)

df = min_max(df)

# Model parameteres
HOLD, BUY_EN, BUY_EX, SELL_EN, SELL_EX = 0, 1, 2, 3, 4

# Actions dictionary
actions_dict = {
    BUY_EN: 'buy_en',
    BUY_EX: 'buy_ex',
    SELL_EN: 'sell_en',
    SELL_EX: 'sell_en',
}

num_actions = len(actions_dict)

# Exploration factor
epsilon = 0.1

# market is a 2d Numpy array of floats between 0.0 to 1.0
# 1.0 corresponds to a free cell, and 0.0 an occupied cell
# rat = (row, col) initial rat position (defaults to (0,0))


# market is a 2d Numpy array of floats between 0.0 to 1.0
# 1.0 corresponds to a free cell, and 0.0 an occupied cell
# rat = (row, col) initial rat position (defaults to (0,0))

class Market(object):
    def __init__(self, prices, trader=0):
        self._market = feat
        self.nsteps = self._market.shape[0]
        self.nsize = self._market.shape[1]
        self.reset(trader)
        self.max_loss = self.account - max_loss_allowed

    def reset(self, trader):
        self.trader = trader
        self.account = account_start
        self.buytrades = dict(tradeopen=0, tradeclose=0)
        self.selltrades = dict(tradeopen=0, tradeclose=0)
        self.portfolio = []  # tradeopen,tradeclose,trade-type,reward
        self.market = np.copy(self._market)
        nsteps = self._market.shape[0]
        step = trader
        self.state = (step, 'free', 'free')
        self.target = training_target
        self.total_reward = 0
        dfog['call'] = 0

    def valid_actions(self, step=None):
        if step is None:
            _, buymode, sellmode = self.state

        actions = [0, 1, 2, 3, 4]

        if buymode == 'buy_en':
            actions.remove(1)
        elif buymode == 'buy_ex':
            actions.remove(2)
        elif sellmode == 'sell_en':
            actions.remove(3)
        elif sellmode == 'sell_ex':
            actions.remove(4)
        elif buymode == 'free':
            actions.remove(2)
        elif sellmode == 'free':
            actions.remove(4)

        return actions

    def update_state(self, action):
        nsteps = self.market.shape[0]
        buytrades = self.buytrades
        selltrades = self.selltrades
        step, buymode, sellmode = self.state
        # print('update_state action:{}'.format(int(action)))
        # print('update_state mode:{}'.format(str(mode)))
        valid_actions = self.valid_actions()
        # print('update_state valid_actions:{}'.format(str(valid_actions)))

        if not valid_actions:
            buymode = 'blocked'
            sellmode = 'blocked'
        elif action in valid_actions:
            if action == BUY_EN and buymode == 'free':
                buytrades['tradeopen'] = dfog.close.values[step]
                dfog.at[step, 'call'] = BUY_EN
                buymode = 'buy_en'
            elif (action == BUY_EX and buymode == 'buy_en') or (step == nsteps - 1 and buymode == 'buy_en'):
                buytrades['tradeclose'] = dfog.close.values[step]
                dfog.at[step, 'call'] = BUY_EX
                buymode = 'buyclose'
            elif action == SELL_EN and sellmode == 'free':
                selltrades['tradeopen'] = dfog.close.values[step]
                dfog.at[step, 'call'] = SELL_EN
                sellmode = 'sell_en'
            elif (action == SELL_EX and sellmode == 'sell_en') or (step == nsteps - 1 and sellmode == 'sell_en'):
                selltrades['tradeclose'] = dfog.close.values[step]
                dfog.at[step, 'call'] = SELL_EX
                sellmode = 'sellclose'
            elif action == HOLD:
                pass

        elif action not in valid_actions and (action == BUY_EN or action == BUY_EX):                  # invalid action, no change in rat position
            buymode = 'invalid-' + buymode
        elif action not in valid_actions and (action == SELL_EN or action == SELL_EX):                  # invalid action, no change in rat position
            sellmode = 'invalid-' + sellmode

        # new state
        # print('update_state mode2:{}'.format(str(nmode)))

        self.state = ((step + 1), buymode, sellmode)
        self.buytrades = buytrades
        self.selltrades = selltrades
        # print('update_state mode3:{}'.format(str(self.state)))

    def get_reward(self):
        step, buymode, sellmode = self.state

        buytradeopen, buytradeclose = self.buytrades['tradeopen'], self.buytrades['tradeclose']
        selltradeopen, selltradeclose = self.selltrades['tradeopen'], self.selltrades['tradeclose']

        if buymode == 'buyclose':
            # print('get_reward tradeopen {} tradeclose {}'.format(tradeopen, tradeclose))
            gross = (buytradeclose - buytradeopen) * leverage
            # print('get_reward gross {}'.format(gross))
            net = gross - commission
            # print('get_reward net {}'.format(net))
            # print('get_reward commission {}'.format(commission))
            self.portfolio.append([buytradeopen, buytradeclose, 'buy', gross, net])  # tradeopen,tradeclose,trade-type,reward
            self.buytrades = dict(tradeopen=0, tradeclose=0)
            buymode = 'free'
            self.state = (step, buymode, sellmode)
            # print('get_reward state: {}'.format(str(self.state)))
            return net
        elif sellmode == 'sellclose':
            # print('get_reward tradeopen {} tradeclose {}'.format(tradeopen, tradeclose))
            gross = (selltradeopen - selltradeclose) * leverage
            # print('get_reward gross {}'.format(gross))
            net = gross - commission
            # print('get_reward net {}'.format(net))
            # print('get_reward commission {}'.format(commission))
            self.portfolio.append([selltradeopen, selltradeopen, 'sell', gross, net])  # tradeopen,tradeclose,trade-type,reward
            self.selltrades = dict(tradeopen=0, tradeclose=0)
            sellmode = 'free'
            self.state = (step, buymode, sellmode)
            # print('get_reward state: {}'.format(str(self.state)))
            return net
        elif buymode[:7] == 'invalid':
            return -10
        elif sellmode[:7] == 'invalid':
            return -10
        else:
            return 0

    def game_status(self):
        if self.account <= self.max_loss:
            return 'lose'
        trader, _, _ = self.state
        nsteps = self.market.shape[0]
        if self.account >= (self.target):
            return 'win'
        if trader == (nsteps - 1):
            return 'steps_over'
        return 'not_over'

    def act(self, action):
        self.update_state(action)
        reward = self.get_reward()
        nstep, buymode, sellmode = self.state
        # print('act self.state: {}'.format(str(self.state)))
        # print('act mode: {}'.format(str(nmode)))
        if buymode[:7] == 'invalid':
            buymode = buymode[8:]
        elif sellmode[:7] == 'invalid':
            sellmode = sellmode[8:]
        else:
            self.account += reward

        self.state = (nstep, buymode, sellmode)
        # print('act mode2:{}\n\n'.format(str(nmode)))
        status = self.game_status()
        # print('action {}. reward {}. state {}. gstatus {}'.format(action, reward, self.state, status))
        envstate = self.observe(tyme=1 if status == 'not_over' else 0)
        return envstate, reward, status

    def observe(self, tyme=0):
        step, _, _ = self.state
        envstate = feat[(step + tyme)]
        envstate = envstate.reshape(1, -1)
        return envstate

    '''
def draw_env(self):
        canvas = np.copy(self.market)
        nrows, ncols = self.maze.shape
        # clear all visual marks
        for r in range(nrows):
            for c in range(ncols):
                if canvas[r, c] > 0.0:
                    canvas[r, c] = 1.0
        # draw the rat
        row, col, valid = self.state
        canvas[row, col] = rat_mark
        return canvas
'''


def show():
    buy_entry = []
    sell_entry = []
    buy_exit = []
    sell_exit = []

    for index, row in dfog.iterrows():
        call = row['call']
        close = row['close']
        if call == BUY_EN:
            buy_entry.append([index, close])
        elif call == BUY_EX:
            buy_exit.append([index, close])
        elif call == SELL_EN:
            sell_entry.append([index, close])
        elif call == SELL_EX:
            sell_exit.append([index, close])

    if not buy_entry:
        buy_entry.append([0, dfog.close[0]])
    if not buy_exit:
        buy_exit.append([0, dfog.close[0]])
    if not sell_entry:
        sell_entry.append([0, dfog.close[0]])
    if not sell_exit:
        sell_exit.append([0, dfog.close[0]])

    buy_entry = np.array(buy_entry)
    buy_exit = np.array(buy_exit)
    sell_entry = np.array(sell_entry)
    sell_exit = np.array(sell_exit)

    dfog['close'].plot(figsize=(16, 6))
    plt.scatter(buy_entry[:, 0], buy_entry[:, 1], c='green')
    plt.scatter(buy_exit[:, 0], buy_exit[:, 1], c='Cyan')
    plt.scatter(sell_entry[:, 0], sell_entry[:, 1], c='red')
    plt.scatter(sell_exit[:, 0], sell_exit[:, 1], c='Magenta')
    plt.show()


def play_game(model, market, step):

    loss = 0.0
    trader = 0  # random.randint(0, int(market.nsteps * 0.3))
    market.reset(trader)
    game_over = False

    win_rate = 0.0
    goodtrades = 0

    # get initial envstate (1d flattened canvas)
    envstate = market.observe()

    n_episodes = 0

    while not game_over:
        prev_envstate = envstate
        # Get next action
        action = np.argmax(experience.predict(prev_envstate))

        # Apply action, get reward and new envstate
        envstate, reward, game_status = market.act(action)
        if game_status == 'win':
            game_over = True
        elif game_status == 'lose':
            game_over = True
        elif game_status == 'steps_over':
            game_over = True
        else:
            game_over = False

    loss = model.evaluate(inputs, targets, verbose=0)

    if len(market.portfolio) > 1:
        for trades in market.portfolio:
            if int(trades[3][0]) > 0:
                goodtrades += 1
        win_rate = (goodtrades / len(market.portfolio))

    template = "Loss: {:.4f} | Episodes: {:d} | Account: {:3f} | Trades: {:d}| Win Ratio: {:3f}"
    print(template.format(loss, n_episodes + 1, market.account, len(market.portfolio), win_rate))
    show()

    if winrate > 0.5 and market.account >= training_target:
        return True
    else:
        return False


def skill_check(model, market):
    play_game()


class Experience(object):
    def __init__(self, model, max_memory=5000, discount=0.95):
        self.model = model
        self.max_memory = max_memory
        self.discount = discount
        self.memory = list()
        self.num_actions = model.output_shape[-1]

    def remember(self, episode):
        # episode = [envstate, action, reward, envstate_next, game_over]
        # memory[i] = episode
        # envstate == flattened 1d maze cells info, including rat cell (see method: observe)
        self.memory.append(episode)
        if len(self.memory) > self.max_memory:
            del self.memory[0]

    def predict(self, envstate):
        return self.model.predict(envstate)[0]

    def get_data(self, data_size=10):
        env_size = self.memory[0][0].shape[1]   # envstate 1d size (1st element of episode)
        mem_size = len(self.memory)
        data_size = min(mem_size, data_size)
        inputs = np.zeros((data_size, env_size))
        targets = np.zeros((data_size, self.num_actions))
        for i, j in enumerate(np.random.choice(range(mem_size), data_size, replace=False)):
            envstate, action, reward, envstate_next, game_over = self.memory[j]
            inputs[i] = envstate
            # There should be no target values for actions not taken.
            targets[i] = self.predict(envstate)
            # Q_sa = derived policy = max quality env/action = max_a' Q(s', a')
            Q_sa = np.max(self.predict(envstate_next))
            if game_over:
                targets[i, action] = reward
            else:
                # reward + gamma * max_a' Q(s', a')
                targets[i, action] = reward + self.discount * Q_sa
        return inputs, targets


# for training steps progress

def qtrain(model, market, **opt):
    global epsilon
    n_epoch = opt.get('n_epoch', 15000)
    max_memory = opt.get('max_memory', 100)
    data_size = opt.get('data_size', 50)
    weights_file = opt.get('weights_file', "")
    name = opt.get('name', 'model')
    start_time = datetime.datetime.now()

    # If you want to continue training from a previous model,
    # just supply the h5 file name to weights_file option
    if weights_file:
        print("loading weights from file: %s" % (weights_file,))
        model.load_weights(weights_file)

    # Construct environment/game from df market (see above)
    market = Market(feat)

    # Initialize experience replay object
    experience = Experience(model, max_memory=max_memory)

    '''
    win_history = []   # history of win/lose game
    n_free_cells = len(qmaze.free_cells)
    hsize = qmaze.maze.size // 2   # history window size
    win_rate = 0.0
    imctr = 1
    '''

    for epoch in range(n_epoch):
        loss = 0.0
        trader = 0  # random.randint(0, int(market.nsteps * 0.3))
        market.reset(trader)
        game_over = False

        win_rate = 0
        goodtrades = 0

        # get initial envstate (1d flattened canvas)
        envstate = market.observe()

        n_episodes = 0
        # imgl = []
        while not game_over:
            prev_envstate = envstate
            # Get next action
            if np.random.rand() < epsilon:
                action = random.randint(0, 2)
            else:
                action = np.argmax(experience.predict(prev_envstate))

            # Apply action, get reward and new envstate
            envstate, reward, game_status = market.act(action)
            if game_status == 'win':
                game_over = True
            elif game_status == 'lose':
                game_over = True
            elif game_status == 'steps_over':
                game_over = True
            else:
                game_over = False

            # Store episode (experience)
            episode = [prev_envstate, action, reward, envstate, game_over]
            experience.remember(episode)
            n_episodes += 1

            # Train neural network model
            inputs, targets = experience.get_data(data_size=data_size)
            h = model.fit(
                inputs,
                targets,
                epochs=8,
                batch_size=16,
                verbose=0,
            )
            loss = model.evaluate(inputs, targets, verbose=0)

        # ani = animation.ArtistAnimation(fig, imgl, interval=50, blit=True, repeat_delay=1000)
        # ani.save('video/Epoch{}.Episodes{}.Winrate{}.mp4'.format(epoch, n_episodes, win_rate), writer=FFwriter)

        if len(market.portfolio) > 0:
            for trades in market.portfolio:
                if trades[4] > 0:
                    goodtrades += 1
            win_rate = (goodtrades / len(market.portfolio))

        dt = datetime.datetime.now() - start_time
        t = format_time(dt.total_seconds())
        template = "Epoch: {:03d}/{:d} | Loss: {:.4f} | Episodes: {:d} | Account: {:2f} | Trades: {:d}| Win Ratio: {:2f} | Average Trades: {:2f} | time: {}"
        print(template.format(epoch, n_epoch - 1, loss, n_episodes + 1, market.account, len(market.portfolio), win_rate, np.mean([i[4] for i in market.portfolio]), t))

        # we simply check if training has exhausted all free cells and if in all
        # cases the agent won

        if np.mean([i[4] for i in market.portfolio]) > 4.0:
            epsilon = 0.05
            show()
        if market.account >= training_target and win_rate >= 0.5:
            show()
            break

    # Save trained model weights and architecture, this will be used by the visualization code
    h5file = "/model/" + name + ".h5"
    json_file = "/model/" + name + ".json"
    model.save_weights(h5file, overwrite=True)
    with open(json_file, "w") as outfile:
        json.dump(model.to_json(), outfile)
    end_time = datetime.datetime.now()
    dt = datetime.datetime.now() - start_time
    seconds = dt.total_seconds()
    t = format_time(seconds)
    print('files: %s, %s' % (h5file, json_file))
    print("n_epoch: %d, max_mem: %d, data: %d, time: %s" % (epoch, max_memory, data_size, t))
    return seconds

# This is a small utility for printing readable time strings:


def format_time(seconds):
    if seconds < 400:
        s = float(seconds)
        return "%.1f seconds" % (s,)
    elif seconds < 4000:
        m = seconds / 60.0
        return "%.2f minutes" % (m,)
    else:
        h = seconds / 3600.0
        return "%.2f hours" % (h,)


def build_model(market, lr=0.001):
    activationChoice = 'relu'
    # model.add(Activation(activationChoice))

    model = Sequential()
    model.add(Dense(240, input_shape=(240,)))
    model.add(PReLU())
    model.add(Dense(128, activation=activationChoice))
    model.add(PReLU())
    model.add(Dense(64, activation=activationChoice))
    model.add(PReLU())
    model.add(Dense(32, activation=activationChoice))
    model.add(PReLU())
    model.add(Dense(num_actions))
    model.compile(optimizer='adam', loss='mse')
    return model


def test():
    market.act(BUY_EN)
    print(market.state)

    market.act(BUY_EN)
    print(market.state)

    market.act(BUY_EX)
    print(market.state)

    market.act(SELL_EN)
    print(market.state)

    market.act(SELL_EN)
    print(market.state)

    market.act(HOLD)
    print(market.state)

    market.act(SELL_EN)
    print(market.state)

    market.act(SELL_EX)
    print(market.state)

    print(market.account)


market = Market(feat)
# test()
model = build_model(market)
qtrain(model, market, epochs=1000, max_memory=8 * 3, data_size=32)
