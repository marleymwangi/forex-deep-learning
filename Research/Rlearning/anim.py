import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.animation as animation


def read_data():
    # get_data()
    file_loc = "data/eurusd.csv"
    dfTemp = pd.read_csv(file_loc, parse_dates=True, na_values=['nan'])
    dfTemp.drop_duplicates()
    #dfTemp.datetime = pd.to_datetime(dfTemp.datetime, format='%Y.%m.%d %H:%M:%S.%f')
    #dfTemp = dfTemp.set_index('datetime')

    return dfTemp


df = read_data()
df = df[:15]
dfog = df.copy()
dfog['call'] = 0
print(dfog)
dfog.at[2, 'call'] = 1
dfog.at[3, 'call'] = 3
dfog.at[4, 'call'] = 1
dfog.at[5, 'call'] = 3

print(dfog)
df.open = (df.close - df.open)
df.high = (df.close - df.high)
df.low = (df.close - df.low)

dfprices = df.close
df.drop('close', axis=1, inplace=True)

# print(dfog)

buy_points = []
sell_points = []

for index, row in dfog.iterrows():
    call = row['call']
    close = row['close']
    if call == 1:
        buy_points.append([index, close])
    elif call == 3:
        sell_points.append([index, close])

    if not buy_points:
        buy_points.append([0, dfog.close[0]])
    if not sell_points:
        sell_points.append([0, dfog.close[0]])


buy_points = np.array(buy_points)
sell_points = np.array(sell_points)
dfog['close'].plot(figsize=(16, 6))
print(buy_points)

plt.scatter(buy_points[:, 0], buy_points[:, 1], c='Magenta')
plt.scatter(sell_points[:, 0], sell_points[:, 1], c='red')
# plt.scatter(buy_points)


plt.show()
