import numpy as np
import pandas as pd


def read_data():
    # get_data()
    file_loc = "data/gbpusd.csv"
    dfTemp = pd.read_csv(file_loc, parse_dates=True, na_values=['nan'])
    dfTemp.drop_duplicates()
    dfTemp.drop('datetime', axis=1, inplace=True)
    # dfTemp.datetime = pd.to_datetime(dfTemp.datetime, format='%Y.%m.%d %H:%M:%S.%f')
    # dfTemp = dfTemp.set_index('datetime')

    return dfTemp


df = read_data()
df = df[:1000]

feat1 = df.values
count = 0
temp = []
feat = []

for item in feat1:
    temp.append(item)
    if count >= 5:
        feat.append(temp)
        temp = temp[1:]
    count += 1

feat = np.array(feat)
print(feat.shape)
print(len(df[5:1000]))
